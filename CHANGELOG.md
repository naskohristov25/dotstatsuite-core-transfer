# HISTORY

## v8.1.0 

### Database compatibility
- StructureDB v6.17
- DataDB v5.6
- CommonDB v3.7 

### Issues
- [#39](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/39) Readme appended with description of changing authentication token claims mapping
- [#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/174) References updated to match Eurostat NSI v8.7.1
- [#189](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/189) Manage Time in group attributes - part 2
- [#275](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/275) Fix for zip file extraction not cleaned from temp folder and "filesCount" added to health page

## v8.0.1 

### Database compatibility
- StructureDB v6.16
- DataDB v5.6
- CommonDB v3.7 

### Issues
- [#76](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/76) Errors found in the AllowMissingComponents SQL migration script for the recreation of the DSD/DF views
- [#77](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/77) Errors found in the ChangeFactTableIndexes SQL migration script for the recreation of the Fact table indexes


## v8.0.0 

### Database compatibility
- StructureDB v6.16
- DataDB v5.6
- CommonDB v3.7 

### Issues
- [#33](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/33) Added TokenUrl param to auth config
- [#75](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/75) Change primary key from PERIOD_SDMX to PERIOD_START and PERIOD_END, fix of semantic error displayed when time period is invalid
- [#113](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/113) Support REPORTING_YEAR_START_DAY attribute, Time range format, date + time format, fixed NumberStyles used at float and double, usage of SUPPORT_DATETIME annotation added
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/161) References updated to match Eurostat NSI v8.5.0
- [#178](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/178) Stream import files
- [#210](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/210) New date format applied on PIT release and restoration dates
- [#212](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/212) Added missing Primary measure representation warning, added logs to returned response for init/dataflow method
- [#214](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/214) Fix of codelist item ids not in consecutive order issue
- [#215](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/215) Allow non reported components
- [#223](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/223) Changed localization text for bulk copy notifications and improved merge performance with full details
- [#226](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/226) Added number of obs to pre-generated actual content constraints
- [#231](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/231) Fill order_by information for mappingsets, recreate DSD and DataFlow views include SID column
- [#236](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/236) Fixes for file validations
- [#237](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/237) Updated mapping set creation function to use new TIME_PRE_FORMATED mapping
- [#242](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/242) New ApplyEmptyDataDbConnectionStringInMSDB configuration parameter added
- [#245](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/245) Fix of column mapping during data transfer; fix missing attributes in destination during transfer
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/251) Corrected the creation of staging table, use fact table info to create time columns, when the fact table exists
- Added added 'disk size' and 'disk space free %' to health check


## v7.2.0 
### Description

### Issues
[#72](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/72) Added support for creation of read-only user to DbUp
[#116](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/116) Added support for Azure SQL Database and Azure SQL Managed Instance to DbUp


## v7.1.0 

### Database compatibility
- StructureDB v6.12 
- DataDB v5.2 
- CommonDB v3.7 

### Issues
[#130](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/130) added temp file cleaning service
[#64](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/64) Skip codes in allowed constraint not part of codelist 
[#44](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/44) Generate empty ACC for live version when first upload targets PIT 
[#170](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/170) remove the requirement that the action can only be performed if the DSD is already deleted from the Mapping Store DB for method /cleanup/dsd  
[#70](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/70) Password change added to DbUp 
[#71](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/71) sql server compatible issues 
[#160](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/160) adjusted attribute validation message for edd import
[#132](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/123) Allow imports with basic and full validations. New validation only functions 
[#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/174) Create transaction item for init/allMappingsets method 
[#201](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/201) Fix of transfer between two different data versions 
[#4](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/4) Update Transfer service error messages 
[#123](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/123) remove primary key in staging table 
[#198](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/198) Initialize empty mappingsets and actual content constraints for the first dataload 
[#205](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/205) Fix bug, actual content constraint not updated after data transfer 
[#202](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/202) Fix of wrong content constraint start date update issue 


## v7.0.0 

### Database compatibility
- StructureDB v6.12 
- DataDB v5.0 
- CommonDB v3.6 

### Issues
[#16](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/16) Bugfix, 4.0 Time dimension without codelist.

## v6.3.0 
### Description

### Issues
[#69](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/69) Bugfix for issue, dbup changes target db to single user mode at start and sets back to multiuser on finishs. 

## v6.1.0 
### Description

### Database compatibility
- StructureDB v6.12 
- DataDB v5.0 
- CommonDB v3.6 

### Issues

[#1](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/1) Remove obsolete/unused ci variables. 
<BR>[#96](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/96) Cleanup mappingsets updated. 
<BR>[#168](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/168) Fix codelist mapping identification of DSD components. 
<BR>[#60](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/60) Csv reader validation error messages.
<BR>[#124](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/124) Non-numeric measure type implementation related changes. 
<BR>[#67](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/67) Add ExecutionTimeout in DbUp.
<BR>[#159](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/159) Manage Time in group attribute.
<BR>[#50](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/50) Storage of non-observation attributes at series and observation level.
<BR>[#51](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/51) Performance improvement � Remove ROW_ID from DSDs with less than 34 dimensions.
<BR>[#84](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/84) Support for DSD without Time dimension. 
<BR>[#16](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/16) Time dimension storage without codelist. 
<BR>[#57](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/57) Change producers to report all non-dataset attributes at obs level.
<BR>[#167](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/167) Validation of data database version.


## v6.0.5 (MSDB v6.12, DataDB v3.5) 
### Description

`WARNING`
Note that if the Data database is in a corrupted state (missing tables) before the migration to a new version, the migration tool (dbup tool) can potentially fail to update the database. We recommend the following steps during the migration:
1.  Backup all databases
2. When the dbup tool is run, please look carefully in the result of the execution (It will prompt you if there were issues during the update).
     - One known issue is that it will skip the creation of internal views when the required tables are not found.
3. If in the previous steps, there are errors for specific DSDs/Dataflows, **Before you do any other action, first use the method */init/dataflow* for the failing dataflows.**
     - If the previous doesn't help/work then the you must delete these dsd/dfs and run the */cleanup/dsd* function for those.
--------
After a successful migration, **Before any other action** you must first run the function */init/allMappingSets* for all the configured data spaces.

### Issues
- [#173](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/173) Added support to get logs of requests not creating transaction entry in db via status request method.


## v6.0.0 (MSDB v6.12, DataDB v3.5) 
### Description

==Important== Before using this software, sure to have backed up databases.
This version is a major update with breaking changes. 

This release contains upgrades of the Eurostat nuget package(s), the changelog for this can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/maapi.net.mirrored/-/blob/master/CHANGELOG.md)

- [#173](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/173) Added support to get logs of requests not creating transaction entry in db via status request method.
- [#103](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/103) References updated to Eurostat NSI v8.1.2.
- [#110](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/110) Add feature to validate the allowed content constraint for coded-attributes transfer-service.
- [#160](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/160) Fix issues with Mappingsets initialisation in transfer service.
- [#63](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/63) Bugfix for issue Exception of type 'DotStat.Db.Exception.KeyValueReadException' in Excel+EDD upload response.
- [#N/A]() Increase the threshold of the performance tests for small imports.
- [#N/A]() Update the documentation of the PIT restoration function.
- [#165](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/165) Bugfix allDataflows method fails with a Timed Out Error.
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/161) Bugfix dataflow views with non mandatory dsd lvl attributes.
- [#120](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/120) Feature to consult the status of the data imports/transactions.
- [#49](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/49) Automatically create mapping sets in the mapping store database.
- [#154](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/154) For transfer, check existence of target dataflow and necessary permissions before responding with transaction ID.
- [#157](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/157) Validate when mappingset valid to date is datemax, set it to datemax - 1 second.
- [#63](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/63) remove check if dataflow already exists on /init/dataflow method, 
- [#N/A]() Added a SourceVersion to the Transfer endpoint. Involved adding an intermediate param, IFromSqlTransferParam, as ISqlTransferParam is used for Sdmx file to Sql as well.
- [#149](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/149) Bugfix in excel upload .
- [#108](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/108) Use Estat PermissionType .
- [#N/A](https://gitlab.com/sis-cc/.stat-suite/keycloak/-/issues/7) Change from implicit flow to Authorization code flow.
- [#143](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/143) Detailed import summary added.
- [#48](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/48) Automatically create data database sql views for dsd and dataflows.
- [#53](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/53) Grant permision to create view to dotstatwriter role.
- [#105](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/105) Use dataReaderManager, fix double unzip.


## 5.0.0 (MSDB v6.9, DataDB v2.1)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/93
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/83
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/79
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/95
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/29
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/78
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/94
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/1
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/96
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/101
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/106
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/111
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/46
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/117
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/100
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/49
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/107
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/101
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/102

## v4.2.4 2020-04-18 (MSDB v6.7, DataDB v2.1)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/65


## v4.2.3 2020-04-18 (MSDB v6.7, DataDB v2.1)

- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/97
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/47
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/84
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/3
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/62
- https://gitlab.com/sis-cc/dotstatsuite-documentation/-/issues/65
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/58
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/65


## v4.1.2 2020-03-30 (MSDB v6.7, DataDB v2.1)

https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/42
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/54
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/75
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/71
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/80
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/88
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/85
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/77
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/86
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/76
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/82
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/91
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/92


##  v4.0.3 2020-01-29 (MSDB v6.7, DataDB v2.1)
This release contains breaking changes with changes to the authentication management.

- /sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/66
- /sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/52


##  v3.0.1 2020-01-22 (MSDB v6.7, DataDB v2.1)
This release contains breaking changes with a new entry in the dataspaces.private.json and the introduction of localization.json via the Dotstat.config nuget package.     

- sis-cc/.stat-suite/dotstatsuite-core-common/issues/102