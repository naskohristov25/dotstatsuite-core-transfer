﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Text.RegularExpressions;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Mapping;
using DotStat.Transfer.Excel.Util;

namespace DotStat.Transfer.Excel.Excel
{
    /// <summary>
    /// This is a utility class, that stores the description of the coordinate translation 
    /// between an excel workbook and StatWorks data point
    /// </summary>
    public class ExcelCoordMappingDescriptor : ExcelCellRangeDescriptor
    {
        public readonly string Name;
        private readonly Dataflow _dataflow;
        public readonly bool ExcludeNulls;
        internal readonly string ValueMapperName;

        public bool IsActive;

        internal readonly BreakdownDimensionExpression TargetWorkbookExpressionExpression;
        internal readonly ValueCellReferenceExpression PrimaryValueExpression;

        private readonly List<SdmxArtifactCellReferenceExpression> _dimensionExpressions;
        private readonly List<SdmxArtifactCellReferenceExpression> _obsAttributeExpressions;

        
        private readonly string _DimMapperName;
        private ObservationMapper _Mapper;
        private ExcelDataDescription _Owner;

        public ExcelCoordMappingDescriptor
            (
            string name,
            Dataflow dataflow,
            IEnumerable<MatchPattern> worksheets,
            CellReferenceExpression topLeft,
            CellReferenceExpression bottomRight,
            bool excludeNulls = false,
            string dimMapperName = null,
            string valueMapperName = null,
            BreakdownDimensionExpression targetWorkbookExpressionExpression = null,
            IEnumerable<SdmxArtifactCellReferenceExpression> dimensionCells = null,
            IEnumerable<SdmxArtifactCellReferenceExpression> attributeCells = null,
            ValueCellReferenceExpression primaryValueExpr = null,
            IEnumerable<ConditionExpression> excludedCellConditions = null,
            IEnumerable<CellReferenceExpression> excludedCells = null,
            IEnumerable<AxisReferenceExpression> excludedRows = null,
            IEnumerable<AxisReferenceExpression> excludedColumns = null
            )
            : base(worksheets, topLeft, bottomRight, excludedCellConditions, excludedCells, excludedRows, excludedColumns)
        {
            Name = name;
            _dataflow = dataflow;
            ExcludeNulls = excludeNulls;
            _DimMapperName = dimMapperName;
            ValueMapperName = valueMapperName;

            _Mapper = null;
            _Owner = null;

            PrimaryValueExpression = primaryValueExpr;

            TargetWorkbookExpressionExpression = targetWorkbookExpressionExpression;
            
            _dimensionExpressions       = new List<SdmxArtifactCellReferenceExpression>(dimensionCells);
            _obsAttributeExpressions    = new List<SdmxArtifactCellReferenceExpression>(attributeCells ?? Enumerable.Empty<SdmxArtifactCellReferenceExpression>());
        }

        internal ExcelDataDescription Owner
        {
            get { return _Owner; }
            set
            {
                if (!Equals(_Owner, value))
                {
                    _Mapper = null;
                    _Owner = value;
                }
            }
        }

        public ObservationMapper Mapper => _Mapper ?? (_Mapper = new ObservationMapper(_dataflow));

        internal IEnumerable<SdmxArtifactCellReferenceExpression> DimensionExpressions => _dimensionExpressions;

        internal IEnumerable<SdmxArtifactCellReferenceExpression> ObsAttributeExpressions => _obsAttributeExpressions;


        public IEnumerable<ExcelCellObservationIterator> BuildIterators(IExcelDataSource dataSource)
        {
            if (dataSource == null)
            {
                throw new InvalidOperationException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelDataSourceNotSet));
            }

            return BuildIncludedWorkSheetList(dataSource).Select(ws => new ExcelCellObservationIterator(this, dataSource, ws));
        }
    }
}