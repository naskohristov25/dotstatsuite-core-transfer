﻿namespace DotStat.Transfer.Excel.Excel
{
    public static class ExcelDataSourceHelper
    {
        public static IExcelDataSource GetExcelDataSource(string workbookFullPath,
            string templatePath = null,
            string password = null,
            bool readOnly = true,
            bool needFormattedTextSupport = false)
        {
            return new EPPlusExcelDataSource(workbookFullPath, templatePath, password, readOnly);

            //return new AsposeExcelDataSource( workbookFullPath, templatePath, password, readOnly );

            ////EpPlus does not support xls, xlsb files as well as cell formatting 
            //// Aspose.Cells is very slow on large excel files
            //return needFormattedTextSupport 
            //    || workbookFullPath.EndsWith( ".xls", StringComparison.InvariantCultureIgnoreCase )
            //    || workbookFullPath.EndsWith(".xlsb", StringComparison.InvariantCultureIgnoreCase)
            //    ? (IExcelDataSource)new AsposeExcelDataSource(workbookFullPath, templatePath, password, readOnly)
            //    : new EPPlusExcelDataSource( workbookFullPath, templatePath, password, readOnly );
        }
    }
}