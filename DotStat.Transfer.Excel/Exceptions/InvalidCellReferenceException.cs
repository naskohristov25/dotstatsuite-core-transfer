﻿using System.Diagnostics.CodeAnalysis;

namespace DotStat.Transfer.Excel.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class InvalidCellReferenceException : UnrecoverableAppException
    {
        public InvalidCellReferenceException()
        {
        }

        public InvalidCellReferenceException(string message) : base(message)
        {
        }

        public InvalidCellReferenceException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}