﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace DotStat.Transfer.Excel.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class SWApplicationException : ApplicationException
    {
        public SWApplicationException() : base()
        {
        }

        public SWApplicationException(string message) : base(message)
        {
        }

        public SWApplicationException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}