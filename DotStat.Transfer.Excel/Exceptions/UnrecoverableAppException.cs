﻿using System.Diagnostics.CodeAnalysis;

namespace DotStat.Transfer.Excel.Exceptions
{
    [ExcludeFromCodeCoverage]
    public class UnrecoverableAppException : System.Exception
    {
        public UnrecoverableAppException() : base()
        {
        }

        public UnrecoverableAppException(string message) : base(message)
        {
        }

        public UnrecoverableAppException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}