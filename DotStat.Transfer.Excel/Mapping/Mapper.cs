﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DotStat.Common.Localization;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Util;

namespace DotStat.Transfer.Excel.Mapping
{
    //todo: make IdentityMapper extend this instead of SWMapper and convert references to all SWMapper to Mapper
    public class Mapper
    {
        public class NameMatching
        {
            public readonly string SourceName;
            public readonly string TargetName;

            public NameMatching(string src, string target)
            {
                SourceName = src;
                TargetName = target;
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    return SourceName.GetHashCode()*31 + TargetName.GetHashCode() + 7;
                }
            }

            public override bool Equals(object obj)
            {
                if (!(obj is NameMatching))
                {
                    return false;
                }

                var oth = (NameMatching)obj;
                return SourceName.IsSameAs(oth.SourceName)
                       && TargetName.IsSameAs(oth.TargetName);
            }
        }

        private sealed class MappingNode
        {
            private readonly string _Key;
            private readonly int _SourcePosition;

            private readonly UpdatableFastIndexer<MappingNode> _MatchNodes;
            private readonly IndexedSet<int> _MatchPositions;
            private string _Id;

            public readonly IIndexer<int> MatchPositions;
            public MappingElement Target;

            public MappingNode Parent { get; private set; }

            public MappingNode(int sourcePosition, string key)
            {
                _SourcePosition = sourcePosition;
                _Key = key;
                _MatchNodes = new UpdatableFastIndexer<MappingNode>();
                _MatchNodes.AddIndex("id", node => node.Id);
                _MatchPositions = new IndexedSet<int>();
                MatchPositions = _MatchPositions;
            }

            private string Id
            {
                get { return _Id ?? (_Id = BuildId(_SourcePosition, _Key)); }
            }

            public bool HasFurtherMappings
            {
                get { return _MatchNodes.Count > 0; }
            }

            public static string BuildId(int pos, string key)
            {
                return KeyBuilder.BuildKey(pos) + (key == null
                    ? string.Empty
                    : key.ToUpperInvariant());
            }

            public override bool Equals(object obj)
            {
                var oth = obj as MappingNode;
                return oth != null && _SourcePosition == oth._SourcePosition && _Key.IsSameAs(oth._Key);
            }

            public override int GetHashCode()
            {
                if (_Key == null)
                {
                    return 0;
                }

                return Id.GetHashCode();
            }

            public MappingNode FindNode(string id)
            {
                MappingNode res;

                if (_MatchNodes.TryGetValue("id", id, out res))
                {
                    return res;
                }

                return null;
            }

            public void AddMatchNode(MappingNode node)
            {
                _MatchNodes.Add(node);
                _MatchPositions.Add(node._SourcePosition);
                node.Parent = this;
            }
        }

        public class MappingElement
        {
            public readonly int Index;
            public readonly string[] KeySource;
            public readonly string[] Projection;
            public readonly int[] ComparisonIndexList;
            private readonly int[] _ProjectionIndexList;
            public readonly bool IsRegEx;

            public MappingElement(int index, string[] keySource, string[] projection, bool isRegEx)
            {
                KeySource = keySource.ToArray();
                Projection = projection.ToArray();
                ComparisonIndexList = KeySource.Select((itm, idx) => idx)
                    .Where(idx => keySource[idx] != null)
                    .ToArray();
                _ProjectionIndexList = Projection.Select((itm, idx) => idx)
                    .Where(idx => projection[idx] != null)
                    .ToArray();
                Index = index;
                IsRegEx = isRegEx;
            }

            public static int FindPrecedence(MappingElement mi1, MappingElement mi2)
            {
                if (mi1.IsRegEx != mi2.IsRegEx)
                {
                    if (mi1.IsRegEx)
                    {
                        return 1;
                    }

                    return -1;
                }

                if (mi1.ComparisonIndexList.Length > mi2.ComparisonIndexList.Length)
                {
                    return -1;
                }

                if (mi1.ComparisonIndexList.Length < mi2.ComparisonIndexList.Length)
                {
                    return 1;
                }

                return mi1.Index.CompareTo(mi2.Index);
            }

            internal bool ProjectInto(string[] source, int[] fixedElemPositions)
            {
                var cnt = 0;

                // ReSharper disable once LoopCanBeConvertedToQuery
                for (var ii = 0; ii < _ProjectionIndexList.Length; ii++)
                {
                    var idx = _ProjectionIndexList[ii];
                    var src = source[idx];
                    var prj = Projection[idx];

                    if (src == null || (fixedElemPositions.Length > 0 && fixedElemPositions.Contains(idx)) ||
                        src.IsSameAs(prj))
                    {
                        cnt++;
                    }
                }

                if (cnt != _ProjectionIndexList.Length)
                {
                    return false;
                }

                for (var ii = 0; ii < _ProjectionIndexList.Length; ii++)
                {
                    var idx = _ProjectionIndexList[ii];
                    source[idx] = Projection[idx];
                }

                return true;
            }

            private static bool AreEqual(MappingElement mi1, MappingElement mi2)
            {
                if (mi1 == null && mi2 == null)
                {
                    return true;
                }

                if (mi1 == null || mi2 == null)
                {
                    return false;
                }

                if (ReferenceEquals(mi1, mi2))
                {
                    return true;
                }

                if ((mi1.Index >= 0 && mi2.Index >= 0 && mi1.Index != mi2.Index) || mi1.IsRegEx != mi2.IsRegEx)
                {
                    return false;
                }

                if (mi1.KeySource.Length != mi2.KeySource.Length || mi1.Projection.Length != mi2.Projection.Length)
                {
                    return false;
                }

                if (mi1.KeySource.Where((ks, idx) => !mi2.KeySource[idx].IsSameAs(ks))
                    .Any() || mi1.Projection.Where((pr, idx) => !mi2.Projection[idx].IsSameAs(pr))
                        .Any())
                {
                    return false;
                }

                return true;
            }

            public override int GetHashCode()
            {
                return Index;
            }

            public override bool Equals(object obj)
            {
                return AreEqual(this, obj as MappingElement);
            }
        }


        public string Name;
        public int FixedTargetCount { get; private set; }

        public readonly IIndexer<MappingElement> MappingItems;
        public readonly IIndexer<string> TargetTemplate;

        public readonly IIndexer<string> SourceNames;
        public readonly IIndexer<string> TargetNames;
        public readonly IIndexer<NameMatching> MatchingNames;

        public readonly int SourceCount;
        public readonly int TargetCount;

        private readonly HashSet<string> _UnMatchingNames;
        private readonly Dictionary<int, int> _MatchingPairs;
        private readonly List<NameMatching> _MatchingNames;
        private readonly string[] _TargetTemplate;
        private readonly List<MappingElement> _MappingItems;

        private MappingNode _RootNode;
        private MappingElement[] _RegexElements;
        private int[] _FixedElementsProjectionIndexList;
        private readonly Dictionary<string, int> _NamesIndex;
        private readonly Dictionary<string, bool> _RegExMatchCache;

        /// <summary>
        /// The source positions are mapped to the target positions by default.
        /// Therefore the source array must be at least as long as target array
        /// minus the fixed elements in the target template.
        /// All the fixed elements in the target array must be set at the end;
        /// </summary>
        /// <param name="targetNames"></param>
        /// <param name="matchingNames">The name pair which shows the direct matchings</param>
        /// <param name="targetTemplate"></param>
        /// <param name="sourceNames"></param>
        public Mapper(string[] sourceNames, string[] targetNames, IEnumerable<NameMatching> matchingNames,
            params string[] targetTemplate)
        {
            if (sourceNames == null || sourceNames.Length == 0)
            {
                throw new ApplicationArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .ExcelMapperSourceNotConsistentWithDefinition));
            }

            if (sourceNames.Any(string.IsNullOrWhiteSpace))
            {
                throw new ApplicationArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .ExcelMapperSourceEmptyColumns));
            }

            if (targetNames == null || targetNames.Length == 0)
            {
                throw new ApplicationArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .ExcelMapperTargetNotConsistentWithDefinition));
            }

            if (targetTemplate != null && targetTemplate.Length != 0 && targetTemplate.Length > targetNames.Length)
            {
                throw new ArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .ExcelMapperTargetTemplateSize));
            }

            if (targetNames.Any(string.IsNullOrWhiteSpace))
            {
                throw new ApplicationArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .ExcelMapperTargetEmptyColumns));
            }

            SourceCount = sourceNames.Length;
            TargetCount = targetNames.Length;
            _TargetTemplate = (targetTemplate == null || targetTemplate.Length == 0
                ? new string[TargetCount]
                : targetTemplate.ToArray());

            if (_TargetTemplate.Length < TargetCount)
            {
                Array.Resize(ref _TargetTemplate, TargetCount);
            }

            FixedTargetCount = _TargetTemplate.Count(t => t != null);
            _MappingItems = new List<MappingElement>();
            MappingItems = _MappingItems.Wrap();
            TargetTemplate = _TargetTemplate.Wrap();
            _FixedElementsProjectionIndexList =
                TargetTemplate.Select((itm, tidx) => tidx).Where(tidx => TargetTemplate[tidx] != null).ToArray();
            SourceNames = new Indexer<string>(sourceNames);
            TargetNames = new Indexer<string>(targetNames);
            var dups = SourceNames.GroupBy(s => s).Where(grp => grp.Count() > 1).Select(grp => grp.Key).ToArray();

            if (dups.Length > 0)
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelMapperDuplicateColumnsInSource),
                    string.Join(", ", dups))
                );
            }

            dups = TargetNames.GroupBy(s => s).Where(grp => grp.Count() > 1).Select(grp => grp.Key).ToArray();

            if (dups.Length > 0)
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelMapperDuplicateColumnsInTarget),
                    string.Join(", ", dups))
                );
            }

            _RegExMatchCache = new Dictionary<string, bool>(StringComparer.InvariantCultureIgnoreCase);
            _MatchingNames = new List<NameMatching>();
            _MatchingPairs = new Dictionary<int, int>();

            if (matchingNames != null)
            {
                foreach (var nameMatching in matchingNames)
                {
                    AddNameMatching(nameMatching.SourceName, nameMatching.TargetName);
                }
            }

            MatchingNames = _MatchingNames.Wrap();
            _UnMatchingNames = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
            _NamesIndex = new Dictionary<string, int>(StringComparer.InvariantCultureIgnoreCase);

            for (var ii = 0; ii < TargetNames.Count; ii++)
            {
                _NamesIndex.Add(TargetNames[ii], ii);
            }
        }

        protected bool HasNameMatchingForSource(int sourceIdx)
        {
            EnsureMappingsAreBuilt();
            return _MatchingPairs.ContainsValue(sourceIdx);
        }

        protected bool HasNameMatchingForTarget(int targetIdx)
        {
            EnsureMappingsAreBuilt();
            return _MatchingPairs.ContainsKey(targetIdx);
        }

        /// <summary>
        /// this will prevent the given name automatically matched if both source and the target have the same column name
        /// </summary>
        /// <param name="name"></param>
        public void AddNameUnMatching(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new ArgumentNullException();
            }

            if (!SourceNames.Contains(name, StringComparer.InvariantCultureIgnoreCase)
                || !TargetNames.Contains(name, StringComparer.InvariantCultureIgnoreCase))
            {
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelMapperUnmatchingName),
                    name)
                );
            }
            _UnMatchingNames.Add(name);
        }

        public void AddNameMatching(string sourceName, string targetName)
        {
            if (sourceName.IsSameAs(targetName))
            {
                return;
            }

            _MatchingNames.RemoveAll(
                pair =>
                    pair.SourceName.IsSameAs(sourceName)
                    || pair.TargetName.IsSameAs(targetName));
            _MatchingNames.Add(new NameMatching(sourceName, targetName));
        }

        private void BuildMatchingPairs()
        {
            _RegExMatchCache.Clear();
            _MatchingPairs.Clear();

            foreach (var mn in _MatchingNames)
            {
                AddNameMatchingIndex(mn.SourceName, mn.TargetName);
            }

            foreach (var tn in TargetNames)
            {
                if (SourceNames.IndexOf(tn, StringComparer.InvariantCultureIgnoreCase) >= 0
                    && !_MatchingNames.Exists(mn => mn.TargetName.IsSameAs(tn))
                    && !_UnMatchingNames.Contains(tn))
                {
                    AddNameMatchingIndex(tn, tn);
                }
            }
        }

        private void AddNameMatchingIndex(string sourceName, string targetName)
        {
            var srcIdx = SourceNames.IndexOf(sourceName, StringComparer.InvariantCultureIgnoreCase);
            var targetIdx = TargetNames.IndexOf(targetName, StringComparer.InvariantCultureIgnoreCase);

            if (srcIdx < 0 || targetIdx < 0)
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelMapperInvalidOrDuplicate),
                    sourceName,
                    targetName)
                );
            }

            foreach (var pair in _MatchingPairs.ToArray()
                .Where(pair => pair.Value == srcIdx || pair.Key == targetIdx))
            {
                _MatchingPairs.Remove(pair.Key);
            }

            _MatchingPairs.Add(targetIdx, srcIdx);
        }

        public virtual void AddMapping(string[] source, string[] target, bool isRegex)
        {
            if (source == null || source.Length > SourceCount)
            {
                throw new ApplicationArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .ExcelMapperSourceMappingNotConsistent));
            }

            if (target == null || target.Length > TargetCount)
            {
                throw new ApplicationArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .ExcelMapperTargetMappingNotConsistent));
            }

            if (source.Length != SourceCount)
            {
                Array.Resize(ref source, SourceCount);
            }

            if (target.Length > TargetCount)
            {
                Array.Resize(ref target, TargetCount);
            }

            if (source.All(s => s == null))
            {
                //new fixed members introduced
                target.ForEach(UpdateTargetTemplate);
            }

            else
            {
                var nm = new MappingElement(-1, source, target, isRegex);

                if (_MappingItems.Contains(nm))
                {
                    return;
                }

                _MappingItems.Add(new MappingElement(_MappingItems.Count, source, target, isRegex));
            }

            _RootNode = null;
        }

        public virtual bool ProjectInto(string[] source, string[] target)
        {
            var res = FixedTargetCount > 0;
            Array.Copy(_TargetTemplate, target, TargetCount);
            EnsureMappingsAreBuilt();
            var node = _RootNode;
            var posList = new Dictionary<MappingNode, int>();
            var ii = 0;

            while (node != null && ii < node.MatchPositions.Count)
            {
                var pos = node.MatchPositions[ii++];
                posList[node] = ii;
                var mem = source[pos];
                var id = MappingNode.BuildId(pos, mem);
                var mn = node.FindNode(id);

                if (mn != null)
                {
                    if (mn.HasFurtherMappings)
                    {
                        node = mn;
                    }

                    else
                    {
                        res |= mn.Target.ProjectInto(target, _FixedElementsProjectionIndexList);
                        node = _RootNode;
                    }
                }

                else if (ii < node.MatchPositions.Count)
                {
                    continue;
                }

                else if (node.Target != null)
                {
                    res |= node.Target.ProjectInto(target, _FixedElementsProjectionIndexList);
                    node = _RootNode;
                }

                else
                {
                    node = node.Parent;

                    if (node == null)
                    {
                        break;
                    }

                    //if( node.Target != null ) {
                    //    res |= node.Target.ProjectInto( target, _FixedElementsProjectionIndexList );
                    //    node = _RootNode;
                    //}
                }

                // ReSharper disable PossibleUnintendedReferenceComparison
                if (!posList.TryGetValue(node, out ii))
                {
                    ii = 0;
                }

                else if (_RootNode != node)
                {
                    // if not a match found
                    while (node.MatchPositions.Count <= ii)
                    {
                        node = node.Parent;

                        if (node == null)
                        {
                            break;
                        }

                        if (node.Target != null)
                        {
                            res |= node.Target.ProjectInto(target, _FixedElementsProjectionIndexList);
                            node = _RootNode;
                        }

                        if (!posList.TryGetValue(node, out ii))
                        {
                            ii = 0;
                            break;
                        }
                    }
                }

                // ReSharper enable PossibleUnintendedReferenceComparison
            }

            if (_RegexElements.Length > 0)
            {
                for (var idx = 0; idx < _RegexElements.Length; idx++)
                {
                    var mi = _RegexElements[idx];
                    var match = true;

                    for (var jj = 0; jj < mi.ComparisonIndexList.Length; jj++)
                    {
                        var pos = mi.ComparisonIndexList[jj];
                        var cv = source[pos];

                        if (cv == null)
                        {
                            match = false;
                            break;
                        }
                        var regex = mi.KeySource[pos];
                        var mkey = regex + "\0" + cv;
                        bool cmatch;
                        if (_RegExMatchCache.TryGetValue(mkey, out cmatch))
                        {
                            if (!cmatch)
                            {
                                match = false;
                                break;
                            }
                        }
                        else
                        {
                            cmatch = Regex.IsMatch(cv, regex, RegexOptions.IgnoreCase);
                            _RegExMatchCache.Add(mkey, cmatch);
                            if (!cmatch)
                            {
                                match = false;
                                break;
                            }
                        }
                    }

                    if (match)
                    {
                        mi.ProjectInto(target, _FixedElementsProjectionIndexList);
                    }
                }
            }

            for (var idx = 0; idx < TargetCount; idx++)
            {
                if (target[idx] == null)
                {
                    int srcIdx;

                    if (_MatchingPairs.TryGetValue(idx, out srcIdx))
                    {
                        target[idx] = source[srcIdx];
                    }

                    //todo: else { throw?
                }
            }

            return res;
        }

        protected void EnsureMappingsAreBuilt()
        {
            if (_RootNode != null)
            {
                return;
            }

            BuildMatchingPairs();
            var items = _MappingItems.ToList();
            items.Sort(MappingElement.FindPrecedence);

            var mappings = new Dictionary<string, MappingNode>(StringComparer.Ordinal);
            var root = new MappingNode(0, null);

            for (var ii = 0; ii < items.Count; ii++)
            {
                var mi = items[ii];

                if (mi.IsRegEx)
                {
                    continue;
                }

                var pos = mi.ComparisonIndexList[0];
                var key = mi.KeySource[pos];
                var id = MappingNode.BuildId(pos, key);
                MappingNode node;

                if (!mappings.TryGetValue(id, out node))
                {
                    node = new MappingNode(pos, key);
                    root.AddMatchNode(node);
                    mappings.Add(id, node);
                }

                for (var jj = 1; jj < mi.ComparisonIndexList.Length; jj++)
                {
                    pos = mi.ComparisonIndexList[jj];
                    key = mi.KeySource[pos];
                    id = MappingNode.BuildId(pos, key);
                    var sn = node.FindNode(id);

                    if (sn == null)
                    {
                        sn = new MappingNode(pos, key);
                        node.AddMatchNode(sn);
                    }

                    node = sn;
                }

                node.Target = mi;
            }

            _RootNode = root;
            _RegexElements = _MappingItems.Where(mi => mi.IsRegEx)
                .ToArray();
        }

        /// <summary>
        /// there is no guarantee that reverse would work. It is up to the caller of this method to ensure that the source is reversable
        /// </summary>
        /// <returns></returns>
        public virtual Mapper GetReverseMapper()
        {
            var res = new Mapper(TargetNames.ToArray(), SourceNames.ToArray(),
                MatchingNames.Select(nm => new NameMatching(nm.TargetName, nm.SourceName)));

            //regex mappings cannot be reversed, and hence ignored;
            foreach (var mi in MappingItems.Where(it => !it.IsRegEx))
            {
                res.AddMapping(mi.Projection, mi.KeySource, false);
            }

            return res;
        }

        protected void UpdateTargetTemplate(int idx, string value)
        {
            _TargetTemplate[idx] = value;
            _FixedElementsProjectionIndexList = TargetTemplate.Select((itm, tidx) => tidx)
                .Where(tidx => TargetTemplate[tidx] != null)
                .ToArray();
            FixedTargetCount = _FixedElementsProjectionIndexList.Length; //_TargetTemplate.Count(v => v != null);
            _RootNode = null;
            _MatchingPairs.Remove(idx);
        }

        public override bool Equals(object obj)
        {
            var oth = obj as Mapper;

            if (oth == null)
            {
                return false;
            }

            if (oth.SourceCount != SourceCount || oth.TargetCount != TargetCount ||
                oth.FixedTargetCount != FixedTargetCount)
            {
                return false;
            }

            if (!Name.IsSameAs(oth.Name))
            {
                return false;
            }

            if (!SourceNames.SetEquals(oth.SourceNames, StringComparer.InvariantCultureIgnoreCase))
            {
                if (MappingItems.Count == 0 && oth.MappingItems.Count == 0)
                {
                    //if there is no mapping, order of source names does not matter
                    var sn = new HashSet<string>(SourceNames, StringComparer.InvariantCultureIgnoreCase);

                    if (!sn.SetEquals(oth.SourceNames))
                    {
                        return false;
                    }
                }

                else
                {
                    return false;
                }
            }

            if (!TargetNames.SetEquals(oth.TargetNames, StringComparer.InvariantCultureIgnoreCase))
            {
                if (MappingItems.Count == 0 && oth.MappingItems.Count == 0 && FixedTargetCount == 0 &&
                    oth.FixedTargetCount == 0)
                {
                    //if there is no mapping, order of target names does not matter
                    var sn = new HashSet<string>(TargetNames, StringComparer.InvariantCultureIgnoreCase);

                    if (!sn.SetEquals(oth.TargetNames))
                    {
                        return false;
                    }
                }

                else
                {
                    return false;
                }
            }

            if (!TargetTemplate.SetEquals(oth.TargetTemplate, StringComparer.InvariantCultureIgnoreCase))
            {
                return false;
            }

            var nm = new HashSet<NameMatching>(MatchingNames);

            if (!nm.SetEquals(oth.MatchingNames))
            {
                return false;
            }

            var mi = new IndexedSet<MappingElement>(MappingItems);

            if (!mi.SetEquals(oth.MappingItems))
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            //todo:
            return 0;
        }

        public string GetFieldValue(string targetName, string[] record)
        {
            int idx;

            if (!_NamesIndex.TryGetValue(targetName, out idx) || idx >= record.Length)
            {
                // no such field, no such value perhaps a missing mapping
                throw new UnrecoverableAppException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelMapperFieldNotFound),
                    targetName)
                );
            }

            return record[idx];
        }
    }
}