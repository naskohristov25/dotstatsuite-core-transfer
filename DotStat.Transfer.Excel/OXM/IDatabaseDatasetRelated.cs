﻿using DotStat.Domain;

namespace DotStat.Transfer.Excel.OXM
{
    public interface IDatabaseDatasetRelated
    {
        Dataflow GetDataset();
    }
}