﻿using System;
using DotStat.Transfer.Excel.Mapping;

namespace DotStat.Transfer.Excel.Reader
{
    public interface IRecordIterator : IDisposable
    {
        /// <summary>
        /// The current position in the data source, 0 if not started reading
        /// </summary>
        int CurrentRecordNo { get; }

        /// <summary>
        /// Return the value of the current record for a field
        /// which is a unique number
        /// </summary>
        /// <param name="pos">the posion of the field</param>
        /// <returns></returns>
        string GetField(int pos);

        /// <summary>
        /// the name of the field for a given position
        /// </summary>
        /// <param name="pos">the position of the field</param>
        /// <returns>the name of the field</returns>
        string GetFieldName(int pos);


        /// <summary>
        /// returns the index of a field
        /// </summary>
        /// <param name="field">the field name to return the index for</param>
        /// <returns></returns>
        int GetFieldIndex(string field);

        /// <summary>
        /// The number of fields in the source
        /// </summary>
        int FieldCount { get; }

        /// <summary>
        /// read the current record and move to the next
        /// </summary>
        /// <returns>true if the read was successful, false if EOF is reached</returns>
        bool Read();

        void Reset();
    }

    public interface IRecordIterator<T> : IRecordIterator where T:class
    {
        V8Mapper<T> Mapper { get; } 
    }
}