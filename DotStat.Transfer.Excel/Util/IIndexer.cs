﻿using System.Collections;
using System.Collections.Generic;

namespace DotStat.Transfer.Excel.Util
{
    public interface IIndexer<T> : ICollection, IEnumerable<T>
    {
        T this[int idx] { get; }

        int IndexOf(T elem);
        bool Contains(T item);
    }

    // ReSharper disable once PossibleInterfaceMemberAmbiguity
    public interface IIndexedSet<T> : IIndexer<T>, ICollection<T>
    {
        void AddRange(IEnumerable<T> items);
        void RemoveAll(IEnumerable<T> items);
    }
}