﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DotStat.Transfer.Excel.Util
{
    public class UpdatableFastIndexer<T> : FastIndexer<T>
    {
        public void Add(T element)
        {
            Elements.Add(element);
            ResetIndices();
        }

        public void AddRange(IEnumerable<T> elements)
        {
            if (elements == null)
            {
                throw new ArgumentNullException();
            }

            var elemList = Elements as List<T>;
            var nl = elements as ICollection<T>;
            if (elemList != null)
            {
                if (nl != null)
                {
                    elemList.Capacity = elemList.Count + nl.Count + 10;
                }
                elemList.AddRange(elements);
                ResetIndices();
                return;
            }

            foreach (T elem in elements)
            {
                Elements.Add(elem);
            }
            ResetIndices();
        }

        public bool Remove(T element)
        {
            var idx = IndexOf(element);
            if (idx >= 0)
            {
                Elements.RemoveAt(idx);
                ResetIndices();
                return true;
            }
            return false;
        }

        public override void Clear()
        {
            Elements.Clear();
            ResetIndices();
        }

        public void RemoveAll(Func<T, bool> func)
        {
            ReLoad(Elements.Where(e => !func(e)).ToLinkedList());
        }
    }
}