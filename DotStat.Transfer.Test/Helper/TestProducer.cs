﻿using System.Linq;
using DotStat.Domain;
using DotStat.Test;
using DotStat.Test.Moq;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Transfer.Test.Helper
{
    public sealed class TestProducer<T> : IProducer<T> where T : TransferParam
    {
        private readonly TestMappingStoreDataAccess _msAccess;
        private readonly int _maxObeservations;

        public TestProducer(int maxObservations, string dsdPath = "sdmx/264D_264_SALDI+2.1.xml")
        {
            _msAccess = new TestMappingStoreDataAccess(dsdPath);
            _maxObeservations = maxObservations;
        }

        public Dataflow GetDataflow(T transferParam, bool throwErrorIfNotFound=true)
        {
            return _msAccess.GetDataflow();
        }

        public TransferContent Process(T transferParam, Dataflow dataflow)
        {
            return new TransferContent()
            {
                Observations = ObservationGenerator.Generate(dataflow, 2010, 2020, _maxObeservations),
                DatasetAttributes = Enumerable.Empty<IKeyValue>()
            };
        }

        public bool IsAuthorized(T transferParam, Dataflow dataflow)
        {
            return true;
        }

        public void Dispose()
        {}
    }
}
