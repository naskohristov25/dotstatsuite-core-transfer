﻿using System.IO;

namespace DotStat.Transfer.Test.Helper
{
    public sealed class TestTempFileManager : ITempFileManagerBase
    {
        public string GetTempFileName(string prefix = null) => Path.GetTempFileName();
    }
}
