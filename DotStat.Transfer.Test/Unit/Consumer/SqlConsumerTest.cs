﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.Repository;
using DotStat.Db.Util;
using DotStat.DB.Util;
using DotStat.Db.Validation;
using DotStat.Db.Validation.SqlServer;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Test;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Param;
using Estat.Sdmxsource.Extension.Constant;
using Moq;
using NUnit.Framework;
using Org.Sdmxsource.Sdmx.Api.Model;
using Org.Sdmxsource.Sdmx.Api.Model.Data;

namespace DotStat.Transfer.Test.Unit.Consumer
{
    [TestFixture]
    public class SqlConsumerTest : SdmxUnitTestBase
    {
        private readonly Mock<IMappingStoreDataAccess> _mappingstore = new Mock<IMappingStoreDataAccess>();
        private readonly Mock<IManagementRepository> _mgmtRepository = new Mock<IManagementRepository>();
        private readonly Mock<IDataStoreRepository> _dataStoreRepository = new Mock<IDataStoreRepository>();
        private readonly Mock<ITransactionRepository> _transRepository = new Mock<ITransactionRepository>();
        private readonly Mock<IAuthorizationManagement> _authorisation = new Mock<IAuthorizationManagement>();
        private readonly Mock<ISqlDatasetAttributeDatabaseValidator> _sqlDatasetAttributeDatabaseValidator = new Mock<ISqlDatasetAttributeDatabaseValidator>();
        private readonly Mock<IDatasetAttributeValidator> _datasetAttributeValidator = new Mock<IDatasetAttributeValidator>();
        private readonly Mock<ISqlKeyableDatabaseValidator> _sqlDatabaseValidator = new Mock<ISqlKeyableDatabaseValidator>();
        private readonly Mock<ICodeTranslator> _codeTranslator = new Mock<ICodeTranslator>();
        private readonly Mock<ISqlTransferParam> _param = new Mock<ISqlTransferParam>();

        private readonly Dataflow _dataflow;
        private readonly TransferContent _content;
        private readonly SqlSdmxConsumer _sqlConsumer;

        public SqlConsumerTest()
        {
            this.Configuration.MaxTransferErrorAmount = 0;

            List<IValidationError> emptyErrorList = new List<IValidationError>();
            List<IValidationError> emptyMergeErrorList = new List<IValidationError>();

            _param.Setup(exp => exp.ValidationType).Returns(ValidationType.ImportWithFullValidation);

            _dataStoreRepository.Setup(x => x.BulkInsertData(It.IsAny<IEnumerable<IObservation>>(),
                It.IsAny<ReportedComponents>(),
                It.IsAny<CodeTranslator>(), It.IsAny<Dataflow>(),
                _param.Object.ValidationType == ValidationType.ImportWithFullValidation, false, out emptyErrorList));
            
            _dataStoreRepository.Setup(x => x.MergeStagingToFactData(It.IsAny<Dsd>(), It.IsAny<ReportedComponents>(), It.IsAny<DbTableVersion>(), It.IsAny<bool>(),out emptyMergeErrorList))
                .Returns(new MergeResult(3, 2, 1));
            
            _dataflow = base.GetDataflow();

            _mappingstore.Setup(x =>
                    x.GetDataflow(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<ResolveCrossReferences>()))
                .Returns(_dataflow);

            _param.Setup(exp => exp.Principal)
                .Returns(new DotStatPrincipal(new ClaimsPrincipal(), new Dictionary<string, string>()));

            _param.Setup(exp => exp.DestinationDataspace).Returns(new DataspaceInternal { Id = "DummySpace" });
            _param.Setup(exp => exp.DestinationDataflow).Returns(_dataflow.Base.MutableInstance);
            _param.Setup(exp => exp.CultureInfo).Returns(CultureInfo.CurrentUICulture);

            _transRepository.Setup(x => x.TryNewTransaction(It.IsAny<int>(), It.IsAny<Dataflow>(), It.IsAny<IMappingStoreDataAccess>()
                    , It.IsAny<TargetVersion>(), It.IsAny<bool>(), It.IsAny<DotStatPrincipal>()))
                .Returns(true);

            _authorisation.Setup(x => x.IsAuthorized(
                It.IsAny<DotStatPrincipal>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<string>(),
                It.IsAny<PermissionType>())
            ).Returns(true);

            _sqlDatasetAttributeDatabaseValidator.Setup(x => x.Validate(
                It.IsAny<DbTableVersion>(),
                It.IsAny<List<Domain.Attribute>>(),
                It.IsAny<IEnumerable<IKeyValue>>(),
                It.IsAny<IEnumerable<IKeyValue>>(),
                It.IsAny<Dataflow>(),
                It.IsAny<int>())).Returns(true);

            _sqlDatasetAttributeDatabaseValidator.Setup(x => x.GetErrors()).Returns(new List<IValidationError>());
            _datasetAttributeValidator.Setup(x => x.GetErrors()).Returns(new List<IValidationError>());
            _sqlDatabaseValidator.Setup(x => x.GetErrors()).Returns(new List<IValidationError>());

            _content = new TransferContent()
            {
                Observations = ObservationGenerator.Generate(_dataflow, 2010, 2020, 100),
                DatasetAttributes = Enumerable.Empty<IKeyValue>(),
                ReportedComponents = new ReportedComponents
                {
                    Dimensions = _dataflow.Dsd.Dimensions.ToList(),
                    IsPrimaryMeasureReported = true,
                    Attributes = _dataflow.Dsd.Attributes.ToList()
                }
            };

            _sqlConsumer = new SqlSdmxConsumer(_mappingstore.Object, _authorisation.Object, this.Configuration);
        }

        [Test]
        public void SaveLiveVersionData()
        {
            _transRepository.Setup(x => x.TryNewTransaction(It.IsAny<int>(), It.IsAny<Dataflow>(), It.IsAny<IMappingStoreDataAccess>()
                    , It.IsAny<TargetVersion>(), It.IsAny<bool>(), It.IsAny<DotStatPrincipal>()))
                .Returns(true);
            
            _mgmtRepository.Reset();

            _param.Setup(exp => exp.TargetVersion).Returns(TargetVersion.Live);

            bool success = _sqlConsumer.Save(
                _param.Object,
                _dataflow,
                _content,
                _mgmtRepository.Object,
                _transRepository.Object,
                _dataStoreRepository.Object,
                _sqlDatasetAttributeDatabaseValidator.Object,
                _datasetAttributeValidator.Object,
                _sqlDatabaseValidator.Object,
                _codeTranslator.Object
                );

            _authorisation.Verify(mock => mock.IsAuthorized(
                            It.IsAny<DotStatPrincipal>(),
                            "DummySpace",
                            _dataflow.AgencyId,
                            _dataflow.Code,
                            _dataflow.Version.ToString(),
                            PermissionType.CanImportData
                        ),
                        Times.Once
                    );
            
            _mgmtRepository.Verify(m=>m.CheckManagementTables(_param.Object.Id));
            _transRepository.Verify(m =>
                m.TryNewTransaction(It.IsAny<int>(), It.IsAny<Dataflow>(), It.IsAny<IMappingStoreDataAccess>()
                    , It.IsAny<TargetVersion>(), It.IsAny<bool>(), It.IsAny<DotStatPrincipal>()));

            //Check that  live version was called and not PIT
            _transRepository.Verify(
                m => m.ApplyPITRelease(It.IsAny<Dataflow>(), It.IsAny<bool>(), It.IsAny<int>(),
                    It.IsAny<IMappingStoreDataAccess>()), Times.Never);

           Assert.IsTrue(success);
        }
        
        [Test]
        public void ExecutePitRelease()
        {
            _mgmtRepository.Setup(exp => exp.GetDsdPITVersion(_dataflow.Dsd)).Returns(DbTableVersion.A);
            _mgmtRepository.Setup(exp => exp.GetDsdPITReleaseDate(_dataflow.Dsd)).Returns(DateTime.MinValue);
            _mgmtRepository.Setup(exp => exp.GetDsdLiveVersion(_dataflow.Dsd)).Returns(DbTableVersion.A);

            _param.Setup(exp => exp.TargetVersion).Returns(TargetVersion.PointInTime);
           
            bool success = _sqlConsumer.Save(
                _param.Object,
                _dataflow,
                _content,
                _mgmtRepository.Object,
                _transRepository.Object,
                _dataStoreRepository.Object,
                _sqlDatasetAttributeDatabaseValidator.Object,
                _datasetAttributeValidator.Object,
                _sqlDatabaseValidator.Object,
                _codeTranslator.Object
            );
            
            _transRepository.Verify(
                m => m.ApplyPITRelease(It.IsAny<Dataflow>(), It.IsAny<bool>(), It.IsAny<int>(),
                    It.IsAny<IMappingStoreDataAccess>()), Times.Once);

            Assert.IsTrue(success);
        }

        [Test]
        public void CopyDataToPitVersion()
        {
            _param.Setup(exp => exp.TargetVersion).Returns(TargetVersion.PointInTime);

            bool success = _sqlConsumer.Save(
                _param.Object,
                _dataflow,
                _content,
                _mgmtRepository.Object,
                _transRepository.Object,
                _dataStoreRepository.Object,
                _sqlDatasetAttributeDatabaseValidator.Object,
                _datasetAttributeValidator.Object,
                _sqlDatabaseValidator.Object,
                _codeTranslator.Object
            );

            _dataStoreRepository.Verify(
                m => m.CopyAttributesToNewVersion(It.IsAny<Dsd>(), It.IsAny<DbTableVersion>()), Times.Once);

            Assert.IsTrue(success);
        }
        
        [Test]
        public new void GetDataflow()
        {
            var df = _sqlConsumer.GetDataflow(_param.Object);

            _mappingstore.Verify(x=> x.GetDataflow(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<ResolveCrossReferences>()), Times.Once);
            
            Assert.AreEqual(_dataflow.FullId, df.FullId);

        }

        [TearDown]
        public void TearDown()
        {
            _transRepository.Invocations.Clear();
            _mgmtRepository.Invocations.Clear();
            _authorisation.Invocations.Clear();
        }
    }
}
