﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.Manager;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.Test;
using DotStat.Test.Moq;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using DotStat.Transfer.Test.Helper;
using DotStatServices.Transfer;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Controllers;
using DotStatServices.Transfer.Model;
using DotStatServices.Transfer.Services;
using Estat.Sdmxsource.Extension.Constant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using NUnit.Framework;

namespace DotStat.Transfer.Test.Unit.Controllers
{
    [TestFixture]
    public class ImportControllerTests : UnitTestBase
    {
        private readonly Mock<IManagementRepository> _mgmtRepositoryMock = new Mock<IManagementRepository>();
        private readonly Mock<IDbManager> _dbManagerMock = new Mock<IDbManager>();
        private readonly Mock<IHttpContextAccessor> _contextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IAuthorizationManagement> _authManagementMock = new Mock<IAuthorizationManagement>();

        private readonly Mock<IConsumer<TransferParam>> _consumerMock = new Mock<IConsumer<TransferParam>>();
        private readonly Mock<IConsumer<SdmxMetadataFileToSqlTransferParam>> _metadataConsumerMock = new Mock<IConsumer<SdmxMetadataFileToSqlTransferParam>>();

        private readonly Mock<IMailService> _mailServiceMock = new Mock<IMailService>();
        private readonly Mock<BackgroundQueue> _backgroundQueueMock = new Mock<BackgroundQueue>();
        private readonly ImportController _controller;
        private readonly AuthConfiguration _authConfig;

        private readonly TestMappingStoreDataAccess _mappingStore;
        private readonly TestMappingStoreDataAccess _mappingStoreWithMetadata;
        private readonly ExcelToSqlTransferManager _excelToSqlTransferManager;
        private readonly SdmxFileToSqlTransferManager _sdmxFileToSqlTransferManager;
        private readonly SdmxMetadataFileToSqlTransferManager _sdmxMetadataFileToSqlTransferManager;
        private readonly UrlToSqlTransferManager _urlToSqlTransferManager;
        private readonly Mock<TempFileManager> _tempFileManager;
        
        private readonly string _dataSpace = "design";
        private readonly Dataflow _dataflow;
        private readonly Dataflow _dataflowWithMetadata;
        private int _obsCount;
        private CancellationToken cancellationToken;
        private readonly string _boundary = $"{Guid.NewGuid():N}";
        private readonly string _contentType;

        public ImportControllerTests()
        {
            this.Configuration.SpacesInternal = new List<DataspaceInternal>()
            {
                new DataspaceInternal() {Id = _dataSpace}
            };

            _authConfig = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };

            // ----------------------------------------------------------------------

            _mappingStore = new TestMappingStoreDataAccess("sdmx/264D_264_SALDI+2.1.xml");
            _dataflow = _mappingStore.GetDataflow();

            _mappingStoreWithMetadata = new TestMappingStoreDataAccess("sdmx/CsvV2.xml");
            _dataflowWithMetadata = _mappingStoreWithMetadata.GetDataflow();

            // Setup required moq objects -------------------------------------------

            _dbManagerMock
                .Setup(x => x.GetManagementRepository(It.IsAny<string>()))
                .Returns(_mgmtRepositoryMock.Object);

            _contextAccessorMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity()));

            _authManagementMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    _dataSpace,
                    _dataflow.AgencyId,
                    _dataflow.Code,
                    _dataflow.Version.ToString(),
                    PermissionType.CanImportData
                ))
                .Returns(true);

            _authManagementMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    _dataSpace,
                    _dataflowWithMetadata.AgencyId,
                    _dataflowWithMetadata.Code,
                    _dataflowWithMetadata.Version.ToString(),
                    PermissionType.CanImportData
                ))
                .Returns(true);

            _consumerMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Dataflow>()
                ))
                .Returns(true);

            _consumerMock
                .Setup(x => x.Save(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Dataflow>(),
                    It.IsAny<TransferContent>()
                ))
                .Returns(true)
                .Callback((TransferParam p, Dataflow d, TransferContent c) => { _obsCount = c.Observations.Count(); });

            _metadataConsumerMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<SdmxMetadataFileToSqlTransferParam>(),
                    It.IsAny<Dataflow>()
                ))
                .Returns(true);

            _metadataConsumerMock
                .Setup(x => x.Save(
                    It.IsAny<SdmxMetadataFileToSqlTransferParam>(),
                    It.IsAny<Dataflow>(),
                    It.IsAny<TransferContent>()
                ))
                .Returns(true)
                .Callback((SdmxMetadataFileToSqlTransferParam p, Dataflow d, TransferContent c) => { _obsCount = c.Observations.Count(); });

            _backgroundQueueMock
                .Setup(x => x.Enqueue(It.IsAny<Func<CancellationToken, Task>>()))
                .Callback((Func<CancellationToken, Task> task) =>
                {
                    task.Invoke(new CancellationToken());
                });

            _tempFileManager = new Mock<TempFileManager>(Configuration, new FormOptions());
            _excelToSqlTransferManager = new ExcelToSqlTransferManager(Configuration, new ExcelProducer(_mappingStore), _consumerMock.Object);
            _sdmxFileToSqlTransferManager = new SdmxFileToSqlTransferManager(Configuration, new SdmxFileProducer(_mappingStore, _tempFileManager.Object), _consumerMock.Object);
            _sdmxMetadataFileToSqlTransferManager = new SdmxMetadataFileToSqlTransferManager(Configuration, new SdmxMetadataFileProducer(_mappingStoreWithMetadata, _tempFileManager.Object), _metadataConsumerMock.Object);
            _urlToSqlTransferManager = new UrlToSqlTransferManager(Configuration, new UrlProducer(_mappingStore), _consumerMock.Object);

            var serviceProvider = ConfigureServices();
            var options = serviceProvider.GetService<IOptions<MvcOptions>>();
            var metadataProvider = new EmptyModelMetadataProvider();
            var modelBinderFactory = new ModelBinderFactory(metadataProvider, options, serviceProvider);

            _contentType = $"multipart/form-data; boundary={_boundary}";
            var httpContext = new DefaultHttpContext();
            httpContext.Request.Headers.Add("Content-Type", _contentType);
            httpContext.Request.ContentType = _contentType;
            httpContext.RequestServices = serviceProvider;

            _controller = new ImportController(
                _dbManagerMock.Object,
                _contextAccessorMock.Object,
                _authManagementMock.Object,
                _excelToSqlTransferManager,
                _sdmxFileToSqlTransferManager,
                _sdmxMetadataFileToSqlTransferManager,
                _urlToSqlTransferManager,
                Configuration,
                _authConfig,
                _mailServiceMock.Object,
                _backgroundQueueMock.Object,
                _tempFileManager.Object
            )
            {
                ControllerContext = new ControllerContext
                {
                    HttpContext = httpContext,
                    ValueProviderFactories = new List<IValueProviderFactory> {Mock.Of<IValueProviderFactory>()},
                },
                MetadataProvider = metadataProvider,
                ModelBinderFactory = modelBinderFactory
            };
        }

        private ServiceProvider ConfigureServices()
        {
            var services = new ServiceCollection();
            services
                .AddMvc(options =>
                {
                    if (_authConfig?.Enabled == true)
                    {
                        options.Filters.Add(new AuthorizeFilter());
                    }
                    options.Filters.Add(new ProducesAttribute("application/json"));
                    options.Filters.Add(new ProducesResponseTypeAttribute(typeof(OperationResult),
                        (int)HttpStatusCode.OK));
                    options.Filters.Add(new ProducesResponseTypeAttribute(typeof(OperationResult),
                        (int)HttpStatusCode.BadRequest));
                    options.Filters.Add(new ProducesResponseTypeAttribute((int)HttpStatusCode.Unauthorized));
                    options.Filters.Add(new ProducesResponseTypeAttribute((int)HttpStatusCode.Forbidden));
                })
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddLogging(config =>
            {
                config.AddDebug();
                config.AddConsole();
            });
            return services.BuildServiceProvider();
        }

        [TearDown]
        public void TearDown()
        {
            _authManagementMock.Invocations.Clear();
            _backgroundQueueMock.Invocations.Clear();
            _consumerMock.Invocations.Clear();
            _metadataConsumerMock.Invocations.Clear();
            _obsCount = 0;
        }

        [Test]
        public async Task ImportExcel()
        {
            var eddFile = FormUpload.GetFile("eddFile.xml", "edd/264D_264_SALDI-no-attributes.xml", "application/xml");
            byte[] eddFileStream;
            await using (var ms = new MemoryStream())
            {
                await eddFile.CopyToAsync(ms, cancellationToken);
                eddFileStream = ms.ToArray();
            }

            var excelFile = FormUpload.GetFile("excelFile.xlsx", "excel/264D_264_SALDI.xlsx",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            byte[] excelFileStream;
            await using (var ms = new MemoryStream())
            {
                await excelFile.CopyToAsync(ms, cancellationToken);
                excelFileStream = ms.ToArray();
            }

            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"eddFile", new FormUpload.FileParameter(eddFileStream, eddFile.Name, eddFile.ContentType)},
                {"excelFile", new FormUpload.FileParameter(excelFileStream, excelFile.Name, excelFile.ContentType)},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            _controller.ControllerContext.HttpContext.Request.Body = body;
            _controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            _controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;

            var result = await _controller.ImportExcel(new ExcelImportParameters()
            {
                dataspace = _dataSpace,
                eddFile = eddFileStream,
                excelFile = excelFileStream,
                lang = "en"
            });

            ImportAssert<ExcelToSqlTransferParam>(_dataflow, result, 100);
        }

        [Test]
        public async Task ValidateImportExcel()
        {
            var eddFile = FormUpload.GetFile("eddFile.xml", "edd/264D_264_SALDI-no-attributes.xml", "application/xml");
            byte[] eddFileStream;
            await using (var ms = new MemoryStream())
            {
                await eddFile.CopyToAsync(ms, cancellationToken);
                eddFileStream = ms.ToArray();
            }

            var excelFile = FormUpload.GetFile("excelFile.xlsx", "excel/264D_264_SALDI.xlsx",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            byte[] excelFileStream;
            await using (var ms = new MemoryStream())
            {
                await excelFile.CopyToAsync(ms, cancellationToken);
                excelFileStream = ms.ToArray();
            }

            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"eddFile", new FormUpload.FileParameter(eddFileStream, eddFile.Name, eddFile.ContentType)},
                {"excelFile", new FormUpload.FileParameter(excelFileStream, excelFile.Name, excelFile.ContentType)},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            _controller.ControllerContext.HttpContext.Request.Body = body;
            _controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            _controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;

            var result = await _controller.ValidateImportExcel(new ExcelImportParameters
            {
                dataspace = _dataSpace,
                eddFile = eddFileStream,
                excelFile = excelFileStream,
                lang = "en"
            });

            ImportAssert<ExcelToSqlTransferParam>(_dataflow, result, 100);
        }

        [TestCase("264D_264_SALDI.csv", "data/264D_264_SALDI.csv", 101, false)]
        [TestCase("CsvV2_without_obs_value.csv", "data/CsvV2_without_obs_value.csv", 4, true)]
        public async Task ImportFromPostedSdmxFile(string name, string fileName, int expectedObsCount, bool isMetadata)
        {
            var postedFile = FormUpload.GetFile(name, fileName, "text/csv");
            byte[] postedFileStream;
            await using (var ms = new MemoryStream())
            {
                await postedFile.CopyToAsync(ms, cancellationToken);
                postedFileStream = ms.ToArray();
            }

            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"file", new FormUpload.FileParameter(postedFileStream, postedFile.Name, postedFile.ContentType)},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            _controller.ControllerContext.HttpContext.Request.Body = body;
            _controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            _controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;
            
            var result = await _controller.ImportSdmxFile(new SdmxImportParameters
            {
                dataspace = _dataSpace,
                lang = "en",
                file = postedFileStream
            });

            ImportAssert<SdmxFileToSqlTransferParam>(isMetadata ? _dataflowWithMetadata : _dataflow, result, expectedObsCount, isMetadata);
        }


        [Test]
        public async Task ValidateImportFromPostedSdmxFile()
        {
            var postedFile = FormUpload.GetFile("264D_264_SALDI.csv", "data/264D_264_SALDI.csv", "text/csv");
            byte[] postedFileStream;
            await using (var ms = new MemoryStream())
            {
                await postedFile.CopyToAsync(ms, cancellationToken);
                postedFileStream = ms.ToArray();
            }

            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"file", new FormUpload.FileParameter(postedFileStream, postedFile.Name, postedFile.ContentType)},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            _controller.ControllerContext.HttpContext.Request.Body = body;
            _controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            _controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;
            
            var result = await _controller.ValidateImportSdmxFile(new SdmxImportParameters()
            {
                dataspace = _dataSpace,
                file = postedFileStream,
                lang = "en"
            });

            ImportAssert<SdmxFileToSqlTransferParam>(_dataflow, result, 101);
        }

        [Test]
        public async Task ImportFromSharedSdmxFile()
        {
            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"filepath", "data/264D_264_SALDI.csv"},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            _controller.ControllerContext.HttpContext.Request.Body = body;
            _controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            _controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;

            var result = await _controller.ImportSdmxFile(new SdmxImportParameters
            {
                dataspace = _dataSpace,
                lang = "en",
                filepath = "data/264D_264_SALDI.csv"
            });

            ImportAssert<SdmxFileToSqlTransferParam>(_dataflow, result, 101);
        }

        [Test]
        public async Task ImportFromUrl()
        {
            // Allow file Scheme for a test
            DotStatServices.Transfer.Extensions
                .AllowedImportSchemes
                .Add(Uri.UriSchemeFile);

            var uri = new Uri(new FileInfo("data/264D_264_SALDI_compact.xml").FullName);

            var postParameters = new Dictionary<string, object>
            {
                {"dataspace", _dataSpace},
                {"filepath", uri.AbsoluteUri},
                {"lang", "en"}
            };

            var formData = FormUpload.GetMultipartFormDataAsync(postParameters, _boundary);
            await using Stream body = new MemoryStream(formData);

            _controller.ControllerContext.HttpContext.Request.Body = body;
            _controller.ControllerContext.HttpContext.Request.Body.Position = 0;
            _controller.ControllerContext.HttpContext.Request.ContentLength = body.Length;
            
            var result = await _controller.ImportSdmxFile(new SdmxImportParameters{
                dataspace=_dataSpace,
                lang= "en",
                filepath=uri.AbsoluteUri
            });

            ImportAssert<UrlToSqlTransferParam>(_dataflow, result, 12);
        }

        private void ImportAssert<T>(IDotStatMaintainable dataflow, ActionResult<OperationResult> result, int expectedReadObsCount, bool isMetadata = false) where T : TransferParam
        {
            // Authorization method is called
            _authManagementMock.Verify(mock => mock.IsAuthorized(
                    It.IsAny<DotStatPrincipal>(),
                    _dataSpace,
                    dataflow.AgencyId,
                    dataflow.Code,
                    dataflow.Version.ToString(),
                    PermissionType.CanImportData
                ),
                Times.Once
            );

            // Background task is executed
            _backgroundQueueMock.Verify(mock => mock.Enqueue(
                    It.IsAny<Func<CancellationToken, Task>>()
                ),
                Times.Once
            );

            if (!isMetadata)
            // Consumer.Save method is called
            {
                _consumerMock.Verify(mock => mock.Save(
                        It.IsAny<T>(),
                        It.IsAny<Dataflow>(),
                        It.IsAny<TransferContent>()
                    ),
                    Times.Once);
            }
            else
            {
                _metadataConsumerMock.Verify(mock => mock.Save(
                        It.IsAny<SdmxMetadataFileToSqlTransferParam>(),
                        It.IsAny<Dataflow>(),
                        It.IsAny<TransferContent>()
                    ),
                    Times.Once);
            }

            Assert.IsNotNull(result);
            Assert.IsNotNull(result.Value);
            Assert.IsInstanceOf<OperationResult>(result.Value);
            Assert.IsTrue(result.Value.Success);
            Assert.AreEqual(expectedReadObsCount, _obsCount);
        }

    }
}
