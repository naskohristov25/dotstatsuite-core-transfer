﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Db.Manager;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.Test;
using DotStat.Test.Moq;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStat.Transfer.Processor;
using DotStat.Transfer.Producer;
using DotStat.Transfer.Test.Helper;
using DotStatServices.Transfer;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Controllers;
using DotStatServices.Transfer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;

namespace DotStat.Transfer.Test.Unit.Controllers
{
    [TestFixture]
    public class TransferControllerTests : UnitTestBase
    {
        private readonly Mock<IHttpContextAccessor> _contextAccessorMock = new Mock<IHttpContextAccessor>();
        private readonly Mock<IDbManager> _dbManagerMock = new Mock<IDbManager>();
        private readonly Mock<IManagementRepository> _mgmtRepositoryMock = new Mock<IManagementRepository>();
        private readonly Mock<IProducer<TransferParam>> _producerMock = new Mock<IProducer<TransferParam>>();
        private readonly Mock<IConsumer<TransferParam>> _consumerMock = new Mock<IConsumer<TransferParam>>();
        private readonly Mock<IMailService> _mailServiceMock = new Mock<IMailService>();
        private readonly Mock<BackgroundQueue> _backgroundQueueMock = new Mock<BackgroundQueue>();
        private readonly ITempFileManagerBase _tempFileManager = new TestTempFileManager();
        private readonly TransferController _controller;
        private readonly AuthConfiguration _authConfig;

        private readonly Dataflow _dataflow;
        private int _obsCount;

        public TransferControllerTests()
        {
            this.Configuration.SpacesInternal = new List<DataspaceInternal>()
            {
                new DataspaceInternal() {Id = "sourceInternalSpace"},
                new DataspaceInternal() {Id = "targetSpace"}
            };

           _authConfig = new AuthConfiguration()
            {
                ClaimsMapping = new Dictionary<string, string>()
            };
            // ----------------------------------------------------------------------

            var mappingStore = new TestMappingStoreDataAccess("sdmx/264D_264_SALDI+2.1.xml");
            _dataflow = mappingStore.GetDataflow();

            // Setup required moq objects -------------------------------------------

            _contextAccessorMock
                .Setup(x => x.HttpContext.User)
                .Returns(new ClaimsPrincipal(new ClaimsIdentity()));

            _dbManagerMock
                .Setup(x => x.GetManagementRepository(It.IsAny<string>()))
                .Returns(_mgmtRepositoryMock.Object);

            _producerMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Dataflow>()
                ))
                .Returns(true);

            var smdxFileProducer = new SdmxFileProducer(mappingStore, _tempFileManager);

            _producerMock
                .Setup(x => x.Process(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Dataflow>()
                ))
                .Returns(smdxFileProducer.Process(new SdmxFileToSqlTransferParam()
                    {
                        DestinationDataspace = this.Configuration.SpacesInternal.FirstOrDefault(),
                        FilePath = "data/264D_264_SALDI.csv"
                    },
                    _dataflow
                ));

            _consumerMock.As<IDataflowManager<SqlToSqlTransferParam>>();

            _consumerMock
                .Setup(x => x.Save(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Dataflow>(),
                    It.IsAny<TransferContent>()
                ))
                .Returns(true)
                .Callback((TransferParam p, Dataflow d, TransferContent c) => { _obsCount = c.Observations.Count(); });

            _backgroundQueueMock
                .Setup(x => x.Enqueue(It.IsAny<Func<CancellationToken, Task>>()))
                .Callback((Func<CancellationToken, Task> task) =>
                {
                    task.Invoke(new CancellationToken());
                });

            // ----------------------------------------------------------------------
            var sqlToSqlManager = new SqlToSqlTransferManager(Configuration, _producerMock.Object, _consumerMock.Object, new NoneTransferProcessor());
            // ----------------------------------------------------------------------

            _controller = new TransferController(
                _contextAccessorMock.Object,
                _dbManagerMock.Object,
                sqlToSqlManager,
                Configuration,
                _authConfig,
                _mailServiceMock.Object,
                _backgroundQueueMock.Object
            );
        }

        [TearDown]
        public void TearDown()
        {
            _obsCount = 0;
            _consumerMock.Invocations.Clear();
            _backgroundQueueMock.Invocations.Clear();
        }

        [Test]
        public void ImportFromInternal()
        {
            SetupConsumer(_dataflow, true);

            var result = Transfer();

            ImportAssert<SqlToSqlTransferParam>(
                result,
                Times.Once(),
                StatusCodes.Status200OK,
                true,
                101
            );
        }

        [Test]
        public void ValidateTransferFromInternal()
        {
            SetupConsumer(_dataflow, true);

            var result = _controller.ValidateTransferDataflow(
                "sourceInternalSpace",
                _dataflow.FullId,
                null,
                "targetSpace",
                _dataflow.FullId,
                "en",
                null,
                null
            );

            ImportAssert<SqlToSqlTransferParam>(
                result,
                Times.Once(),
                StatusCodes.Status200OK,
                true,
                101
            );
        }
        [TestCase(TargetVersion.Live)]
        [TestCase(TargetVersion.PointInTime)]
        public void ImportFromInternalNoSourceVersion(TargetVersion targetVersion)
        {
            var mappingStore = new TestMappingStoreDataAccess("sdmx/264D_264_SALDI+2.1.xml");
            
            var smdxFileProducer = new SdmxFileProducer(mappingStore, _tempFileManager);

            SqlToSqlTransferParam actualTransferParam = null;
            _producerMock
                .Setup(x => x.Process(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Dataflow>()
                ))
                .Returns(smdxFileProducer.Process(new SdmxFileToSqlTransferParam()
                    {
                        DestinationDataspace = this.Configuration.SpacesInternal.FirstOrDefault(),
                        FilePath = "data/264D_264_SALDI.csv"
                    },
                    _dataflow
                ))
                .Callback<TransferParam, Dataflow>((param, df)=>
                {
                    actualTransferParam = param as SqlToSqlTransferParam;
                });


            SetupConsumer(_dataflow, true);

            Transfer(targetVersion);

            Assert.AreEqual(TargetVersion.Live, actualTransferParam.SourceVersion); // Regardless of what is provided for targetVersion, the sourceVersion should default to Live.
        }


        [Test]
        public void DestinationDataflowDoesntExists()
        {
            SetupConsumer(null, true);

            var result = Transfer();

            ImportAssert<SqlToSqlTransferParam>(
                result,
                Times.Never(),
                StatusCodes.Status400BadRequest,
                false,
                0
            );
        }

        [Test]
        public void NotAuthorized()
        {
            SetupConsumer(_dataflow, false);

            var result = Transfer();

            ImportAssert<SqlToSqlTransferParam>(
                result,
                Times.Never(),
                StatusCodes.Status403Forbidden,
                false,
                0
            );
        }


        private void ImportAssert<T>(
            ActionResult<OperationResult> result,
            Times bgJobExecution,
            int expectedStatus,
            bool expectedSuccess,
            int expectedReadObsCount
        ) where T : TransferParam
        {
            // Background task is executed
            _backgroundQueueMock.Verify(mock => mock.Enqueue(
                    It.IsAny<Func<CancellationToken, Task>>()
                ),
                bgJobExecution
            );

            // Consumer.Save method is called
            _consumerMock.Verify(mock => mock.Save(
                    It.IsAny<T>(),
                    It.IsAny<Dataflow>(),
                    It.IsAny<TransferContent>()
                ),
                bgJobExecution
            );

            Assert.IsNotNull(result);

            var resultSummary = result.Value ?? ((ObjectResult)result.Result).Value as OperationResult;

            if (result.Result != null)
            {
                Assert.AreEqual(expectedStatus, ((ObjectResult)result.Result).StatusCode);
            }

            Assert.IsNotNull(resultSummary);
            Assert.AreEqual(expectedSuccess, resultSummary.Success);
            Assert.AreEqual(expectedReadObsCount, _obsCount);
        }


        private ActionResult<OperationResult> Transfer(TargetVersion? targetVersion=null)
        {
            return _controller.TransferDataflow(
                "sourceInternalSpace",
                _dataflow.FullId,
                null,
                "targetSpace",
                _dataflow.FullId,
                "en",
                null,
                targetVersion,
                null,
                false,
                null
            );
        }
        
        private void SetupConsumer(Dataflow dataflow, bool isAuthorized)
        {
            _consumerMock.As<IDataflowManager<SqlToSqlTransferParam>>()
                .Setup(x => x.GetDataflow(It.IsAny<SqlToSqlTransferParam>(), It.IsAny<bool>()))
                .Returns(dataflow);

            _consumerMock
                .Setup(x => x.IsAuthorized(
                    It.IsAny<TransferParam>(),
                    It.IsAny<Dataflow>()
                ))
                .Returns(isAuthorized);
        }

    }
}
