﻿using System;
using DotStat.Test;
using DotStatServices.Transfer;
using NUnit.Framework;

namespace DotStat.Transfer.Test.Unit.Utilities
{
    [TestFixture]
    public class ExtensionTests : UnitTestBase
    {
        [TestCase("", true)]
        [TestCase(null, true)]
        [TestCase("2022-06-04T10:16:01", true)]
        [TestCase("2022-06-04T08:16:01Z", true)]
        [TestCase("2022-06-04T09:46:01+01:30", true)]
        [TestCase("2022-06-04T10:16:01.123", true)]
        [TestCase("2022-06-04T08:16:01.123Z", true)]
        [TestCase("2022-06-04T09:46:01.123+01:30", true)]
        [TestCase("2022-06-03T20:16:01-14:00", true)]
        [TestCase("2022-06-03T20:16:01-1400", false)]
        [TestCase("2022-06-04T24:00:00", false)] // DateTime does not support 24th hour in accordance with ISO ISO 8601-1:2019: it is explicitly disallowed by the 2019 revision.
        [TestCase("2022-06-04T10:16:0", false)]
        [TestCase("2022-06-04T09:46:01.1230+01:30", false)]
        [TestCase("2022-06-04T09:46:01.+01:30", false)]
        [TestCase("06-24-2021 23:22:21", false)]
        [TestCase("24-06-2021 23:22:21", false)]
        [TestCase("06/24/2021 23:22:21", false)]
        [TestCase("24/06/2021 23:22:21", false)]
        [TestCase("2021-06-24T25:22:21", false)]
        [TestCase("0900-06-24T23:22:21", false)]
        [TestCase("900-06-24T23:22:21", false)]
        [TestCase("2021-16-24T23:22:21", false)]
        [TestCase("2021-02-31T23:22:21", false)]
        [TestCase("2021-06-24T25:22:21", false)]
        [TestCase("2021-06-24T23:60:21", false)]
        [TestCase("2021-06-24T23:22:60", false)]
        public void GetPitReleaseDateTest(string dateTimeStr, bool isValidExpected)
        {
            DateTime? dateTimeValue = null;
            if (isValidExpected)
            {
                Assert.DoesNotThrow(() => dateTimeValue = dateTimeStr.GetPITReleaseDate("en"));

                if (string.IsNullOrEmpty(dateTimeStr))
                {
                    Assert.IsNull(dateTimeValue);
                }
                else
                {
                    Assert.IsNotNull(dateTimeValue);
                }
            }
            else
            {
                Assert.Throws<ArgumentException>(() => dateTimeValue = dateTimeStr.GetPITReleaseDate("en"));
            }
        }
    }
}
