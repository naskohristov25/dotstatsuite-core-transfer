﻿using DotStat.Domain;
using DotStat.Transfer.Param;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using DotStat.MappingStore;
using Org.Sdmxsource.Sdmx.DataParser.Engine.Csv;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Translator;

namespace DotStat.Transfer.Consumer
{
    public class SdmxCsvConsumer<T> : IConsumer<T> where T : TransferParam, ISdmxCsvTransferParam
    {
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly string _outputCsvFile;

        public SdmxCsvConsumer(IMappingStoreDataAccess mappingStoreDataAccess, string outputCsvFile)
        {
            _outputCsvFile = outputCsvFile;
            _mappingStoreDataAccess = mappingStoreDataAccess;
        }

        public bool Save(T transferParam, Dataflow dataflow, TransferContent transferContent, int maxErrorCount)
        {
            var csvOptions = new SdmxCsvOptions(transferParam.Delimiter)
            {
                UseLabels = transferParam.UseLabels
            };

            // todo: Initialize available Languages !
            var preferedLanguageTranslator = new PreferedLanguageTranslator(
                    new List<CultureInfo>()
                    {
                        CultureInfo.GetCultureInfo("it"),
                        CultureInfo.GetCultureInfo("fr"),
                        CultureInfo.GetCultureInfo("en")
                    },
                    new List<CultureInfo>()
                    {
                        CultureInfo.GetCultureInfo("fr"),
                        CultureInfo.GetCultureInfo("en")
                    },
                    CultureInfo.GetCultureInfo("en")
                );

            using (Stream writer = File.Create(_outputCsvFile))
            {
                var observationConceptId = dataflow.Dsd.TimeDimension.Code;

                using (var dataWriter = new CsvDataWriterEngine(writer, _mappingStoreDataAccess.GetSuperObjectRetrievalManager(), csvOptions, preferedLanguageTranslator))
                {
                    dataWriter.StartDataset(dataflow.Base, dataflow.Dsd.Base, null);

                    foreach (var observation in transferContent.Observations)
                    {
                        var seriesKey = observation.SeriesKey;
                        
                        // start series
                        dataWriter.StartSeries();

                        // series key
                        foreach (var dim in seriesKey.Key)
                            dataWriter.WriteSeriesKeyValue(dim.Concept, dim.Code);

                        // series level attributes
                        if (seriesKey.Attributes != null)
                            foreach (var seriesAttribute in seriesKey.Attributes)
                                dataWriter.WriteAttributeValue(seriesAttribute.Concept, seriesAttribute.Code);

                        dataWriter.WriteObservation(observationConceptId, observation.ObsTime, observation.ObservationValue);

                        // observation level attributes
                        if (seriesKey.Attributes != null)
                            foreach (var obsAttribute in observation.Attributes)
                                dataWriter.WriteAttributeValue(obsAttribute.Concept, obsAttribute.Code);
                    }
                }
            }

            return true;
        }

        public bool IsAuthorized(TransferParam transferParam, Dataflow dataflow)
        {
            //As the Destination is CSV there is no need to check if the User can write Data.
            return true;
        }
    }
}