﻿using DotStat.Common.Auth;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Param;
using Estat.Sdmxsource.Extension.Constant;

namespace DotStat.Transfer.Consumer
{
    public abstract class SqlConsumer : IConsumer<ITransferParam>, IDataflowManager<ISqlTransferParam>
    {
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly IAuthorizationManagement _authorizationManagement;

        protected SqlConsumer(IMappingStoreDataAccess mappingStoreDataAccess, IAuthorizationManagement authorizationManagement)
        {
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _authorizationManagement = authorizationManagement;
        }

        public abstract bool Save(ITransferParam transferParam, Dataflow dataflow, TransferContent transferContent);

        public Dataflow GetDataflow(ISqlTransferParam transferParam, bool throwErrorIfNotFound= true)
        {
            var dataflow = _mappingStoreDataAccess.GetDataflow(
                transferParam.DestinationDataspace.Id,
                transferParam.DestinationDataflow.AgencyId,
                transferParam.DestinationDataflow.Id,
                transferParam.DestinationDataflow.Version,
                throwErrorIfNotFound
            );

            //Set log dataspaceId and TransactionId?
            Log.Notice( string.Format(
                    LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.DataflowLoaded,
                        transferParam.CultureInfo.TwoLetterISOLanguageName),
                    dataflow.FullId,
                    transferParam.DestinationDataspace.Id));

            return dataflow;
        }

        public bool IsAuthorized(ITransferParam transferParam, Dataflow dataflow)
        {
            return _authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                dataflow.AgencyId,
                dataflow.Base.Id,
                dataflow.Version.ToString(),
                PermissionType.CanImportData
            );
        }
    }
}