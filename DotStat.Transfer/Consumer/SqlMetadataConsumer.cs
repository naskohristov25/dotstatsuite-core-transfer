﻿using DotStat.Common.Auth;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Param;

namespace DotStat.Transfer.Consumer
{
    public class SqlMetadataConsumer : SqlConsumer
    {
        public SqlMetadataConsumer(IMappingStoreDataAccess mappingStoreDataAccess, IAuthorizationManagement authorizationManagement) : base(mappingStoreDataAccess, authorizationManagement)
        {
        }

        public override bool Save(ITransferParam transferParam, Dataflow dataflow, TransferContent transferContent)
        {
            foreach (var observation in transferContent.Observations)
            {
                
            }

            return true;
        }
    }
}