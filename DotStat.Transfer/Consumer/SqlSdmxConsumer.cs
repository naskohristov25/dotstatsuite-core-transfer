﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Exception;
using DotStat.Db.Repository;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Util;
using DotStat.Db.Validation;
using DotStat.Db.Validation.SqlServer;
using DotStat.DB.Util;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace DotStat.Transfer.Consumer
{
    public class SqlSdmxConsumer : SqlConsumer
    {
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly IGeneralConfiguration _generalConfiguration;

        public SqlSdmxConsumer(IMappingStoreDataAccess mappingStoreDataAccess, IAuthorizationManagement authorizationManagement, IGeneralConfiguration generalConfiguration) : base (mappingStoreDataAccess, authorizationManagement)
        {
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _generalConfiguration = generalConfiguration;
        }

        [ExcludeFromCodeCoverage]
        public override bool Save(ITransferParam transferParam, Dataflow dataflow, TransferContent transferContent)
        {
            // TODO: refactor to use DI
            var mgmtRepository = new SqlManagementRepository(transferParam.DestinationDataspace, _generalConfiguration);
            var transRepository = new SqlTransactionRepository(transferParam.DestinationDataspace, _generalConfiguration);
            var dataStoreRepository = new SqlDataStoreRepository(transferParam.DestinationDataspace, _generalConfiguration);

            var codeTranslator = new CodeTranslator(mgmtRepository);

            var dotStatDb = mgmtRepository.GetDb(); // Gets the default datastore db

            var fullValidation = transferParam.ValidationType == ValidationType.FullValidationOnly ||
                                 transferParam.ValidationType == ValidationType.ImportWithFullValidation;

            var datasetAttributeDatabaseValidator = new SqlDatasetAttributeDatabaseValidator(mgmtRepository, dotStatDb, _generalConfiguration, fullValidation);
            var datasetAttributeValidator = new DatasetAttributeValidator(_generalConfiguration, fullValidation);
            var keyableDatabaseValidator = new SqlKeyableDatabaseValidator(mgmtRepository, dotStatDb, transferContent.ReportedComponents.Attributes, dataflow, codeTranslator, _generalConfiguration.MaxTransferErrorAmount);

            return Save(transferParam, dataflow, transferContent, mgmtRepository, transRepository, dataStoreRepository,
                datasetAttributeDatabaseValidator, datasetAttributeValidator, keyableDatabaseValidator,
                codeTranslator);
        }

        public bool Save(
            ITransferParam transferParam, 
            Dataflow dataflow, 
            TransferContent transferContent, 
            IManagementRepository mgmtRepository,
            ITransactionRepository transRepository,
            IDataStoreRepository dataStoreRepository,
            ISqlDatasetAttributeDatabaseValidator datasetAttributeDatabaseValidator,
            IDatasetAttributeValidator datasetAttributeValidator,
            IKeyableDatabaseValidator keyableDatabaseValidator,
            ICodeTranslator codeTranslator)
        {
            if (!IsAuthorized(transferParam, dataflow))
            {
                throw new TransferUnauthorizedException();
            }

            //Check if Repository objects are instantiated, use fluent validator 
            if (mgmtRepository == null)
            {
                throw new ArgumentNullException(nameof(mgmtRepository));
            }

            if (transRepository == null)
            {
                throw new ArgumentNullException(nameof(transRepository));
            }

            if (dataStoreRepository == null)
            {
                throw new ArgumentNullException(nameof(dataStoreRepository));
            }

            if (datasetAttributeDatabaseValidator == null)
            {
                throw new ArgumentNullException(nameof(datasetAttributeDatabaseValidator));
            }

            if (datasetAttributeValidator == null)
            {
                throw new ArgumentNullException(nameof(datasetAttributeValidator));
            }

            if (keyableDatabaseValidator == null)
            {
                throw new ArgumentNullException(nameof(keyableDatabaseValidator));
            }

            if (codeTranslator == null)
            {
                throw new ArgumentNullException(nameof(codeTranslator));
            }

            mgmtRepository.CheckManagementTables(transferParam.Id);

            try
            {
                //In case there is already a PIT release defined on the DSD, 
                //all transaction on this DSD have to target PIT release irrespectively to the value of this parameter.
                if (transferParam.TargetVersion == TargetVersion.Live && mgmtRepository.GetActualDsdPITVersion(dataflow.Dsd) == null)
                    transferParam.TargetVersion = TargetVersion.Live;
                else
                    transferParam.TargetVersion = TargetVersion.PointInTime;

                //check transaction and create one if none exists for the dsd of dataflow.
                //also creates tables/rows for missing artefacts and new data version, populate dataflow and children with database id-s 
                if (!transRepository.TryNewTransaction(transferParam.Id, dataflow, _mappingStoreDataAccess, transferParam.TargetVersion, false, transferParam.Principal))
                {
                    transRepository.CleanUpFailedTransaction(transferParam.Id);
                    return false;
                }

                //The dataflow gets populated in TryNewTransaction, update codeTranslator codelists
                codeTranslator.FillDict(dataflow);

                dataflow.Dsd.LiveVersion = (char?)mgmtRepository.GetDsdLiveVersion(dataflow.Dsd);
                dataflow.Dsd.PITVersion = (char?)mgmtRepository.GetDsdPITVersion(dataflow.Dsd);
                dataflow.Dsd.PITReleaseDate = mgmtRepository.GetDsdPITReleaseDate(dataflow.Dsd);

                //Execute PIT Release if there is one pending 
                if (dataflow.Dsd.PITVersion != null //Is there a PIT?
                    && (dataflow.Dsd.PITReleaseDate != null && dataflow.Dsd.PITReleaseDate <= DateTime.Now)) //PIT release date has passed
                {
                    var previousPITReleaseDate = dataflow.Dsd.PITReleaseDate;

                    transRepository.ApplyPITRelease(dataflow, transferParam.PITRestorationAllowed, transferParam.Id, _mappingStoreDataAccess);

                    if (!dataflow.Dsd.LiveVersion.HasValue)
                    {
                        throw new ArgumentNullException(nameof(dataflow.Dsd.LiveVersion), 
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoValueSetForDsdLiveVersion)
                        );
                    }

                    var newDbTargetVersion = transferParam.TargetVersion == TargetVersion.Live
                        ? (DbTableVersion)dataflow.Dsd.LiveVersion
                        : DbTableVersions.GetNewTableVersion((DbTableVersion)dataflow.Dsd.LiveVersion);

                    mgmtRepository.UpdateTargetVersionOfTransaction(transferParam.Id, newDbTargetVersion);

                    Log.Debug(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.TargetVersionInDBChanged),
                        transferParam.TargetVersion, newDbTargetVersion, previousPITReleaseDate.ToString())
                    );
                }

                if (transferParam.TargetVersion != TargetVersion.Live)
                {
                    dataflow.Dsd.PITReleaseDate = transferParam.PITReleaseDate;
                }

                DbTableVersion targetTableVersion;

                //Copy data to PIT version, for the first pit load.
                if (transferParam.TargetVersion == TargetVersion.PointInTime && dataflow.Dsd.PITVersion == null)
                {
                    //Assign new table version to PITVersion
                    dataflow.Dsd.PITVersion = dataflow.Dsd.LiveVersion == null ?
                        (char)DbTableVersion.A : (char)DbTableVersions.GetNewTableVersion((DbTableVersion)dataflow.Dsd.LiveVersion);
                    targetTableVersion = (DbTableVersion)dataflow.Dsd.PITVersion;

                    if (transferParam.PITRestorationAllowed)
                        dataflow.Dsd.PITRestorationDate = DateTime.Now;
                    //Cleanup target PIT tables
                    dataStoreRepository.DeleteData(dataflow.Dsd, targetTableVersion);
                    //Copy data to new data version
                    dataStoreRepository.CopyDataToNewVersion(dataflow.Dsd, targetTableVersion);
                    //Copy attributes to new data version
                    dataStoreRepository.CopyAttributesToNewVersion(dataflow.Dsd, targetTableVersion);
                }
                else if (transferParam.TargetVersion == TargetVersion.PointInTime && dataflow.Dsd.PITVersion != null)
                {
                    targetTableVersion = (DbTableVersion)dataflow.Dsd.PITVersion;
                    dataflow.Dsd.PITRestorationDate = mgmtRepository.GetDsdPITRestorationDate(dataflow.Dsd);
                }
                else
                {
                    //First data load to live version
                    if (dataflow.Dsd.LiveVersion == null)
                        //Assign new table version to liveVersion
                        dataflow.Dsd.LiveVersion = dataflow.Dsd.PITVersion == null ?
                        (char)DbTableVersion.A : (char)DbTableVersions.GetNewTableVersion((DbTableVersion)dataflow.Dsd.PITVersion);

                    targetTableVersion = (DbTableVersion)dataflow.Dsd.LiveVersion;
                }

                var isTimeAtTimeDimensionSupported = mgmtRepository.CheckSupportOfTimeAtTimeDimension(dataflow.Dsd, targetTableVersion);

                // Wipe and/or create staging table and target tables
                dataStoreRepository.RecreateStagingTables(dataflow.Dsd, transferContent.ReportedComponents, isTimeAtTimeDimensionSupported);

                var maxErrorCount = _generalConfiguration.MaxTransferErrorAmount;

                // Load observation data and attributes into staging table
                // Validate observation and attributes in the import file
                var attributesReportedAtObservation =
                    dataStoreRepository.BulkInsertData(transferContent.Observations, transferContent.ReportedComponents, codeTranslator as CodeTranslator,
                        dataflow, transferParam.ValidationType != ValidationType.ImportWithBasicValidation, isTimeAtTimeDimensionSupported,
                        out var errorList);

                //TODO fine tune, or make it configurable, the amount of observations at which 
                //At which is beneficial to add an index to the staging table
                if (dataStoreRepository.ProcessedObservationsCount()>=3000000 && transferParam.ValidationType != ValidationType.FullValidationOnly)
                    dataStoreRepository.AddIndexStagingTable(dataflow.Dsd, transferContent.ReportedComponents);

                IEnumerable<IKeyValue> datasetAttributeKeyValues = transferContent.DatasetAttributes?.ToList();
                var reportedAttributes = transferContent.ReportedComponents.Attributes;

                // Validate dataset lvl attributes in the import file
                if (errorList.Count < maxErrorCount || maxErrorCount == 0)
                {
                    datasetAttributeValidator.Validate(reportedAttributes,datasetAttributeKeyValues,
                        attributesReportedAtObservation?.DatasetAttributes, dataflow,
                        maxErrorCount > 0 ? maxErrorCount - errorList.Count : 0);

                    errorList.AddRange(datasetAttributeValidator.GetErrors());
                }
                
                // Validate non-dataset lvl attributes values in staging table and in database
                if(transferParam.ValidationType != ValidationType.ImportWithBasicValidation )
                {
                    if (errorList.Count < maxErrorCount || maxErrorCount == 0)
                    {
                        keyableDatabaseValidator.Validate(targetTableVersion,
                            maxErrorCount > 0 ? maxErrorCount - errorList.Count : 0);
                        errorList.AddRange(keyableDatabaseValidator.GetErrors());
                    }
                }

                //Validate dataset lvl attributes database
                if (errorList.Count < maxErrorCount || maxErrorCount == 0)
                {
                    datasetAttributeDatabaseValidator.Validate(targetTableVersion, reportedAttributes, datasetAttributeKeyValues,
                        attributesReportedAtObservation?.DatasetAttributes, dataflow,
                        maxErrorCount > 0 ? maxErrorCount - errorList.Count : 0);

                    errorList.AddRange(datasetAttributeDatabaseValidator.GetErrors());
                }

                if (errorList.Count > 0)
                {
                    throw new ConsumerValidationException(errorList);
                }

                var mergeResult = new MergeResult();
                if (transferParam.ValidationType != ValidationType.FullValidationOnly)
                {
                    //Merge data only for imports/transfer
                    var includeSummary = transferParam.ValidationType == ValidationType.ImportWithFullValidation;
                    mergeResult = dataStoreRepository.MergeStagingToFactData(dataflow.Dsd, transferContent.ReportedComponents, targetTableVersion, includeSummary, out var mergeErrors);

                    if (mergeErrors.Count > 0)
                    {
                        errorList.AddRange(mergeErrors);
                        throw new ConsumerValidationException(errorList);
                    }

                    //Merge attributes only for imports/transfer 
                    dataStoreRepository.MergeStagingToDatasetAttributes(dataflow, datasetAttributeKeyValues,
                        attributesReportedAtObservation?.DatasetAttributes, codeTranslator as CodeTranslator,
                        targetTableVersion);
                }

                //set transaction ready for validation
                mgmtRepository.MarkTransactionReadyForValidation(transferParam.Id);

                //Clean restoration
                if (transferParam.TargetVersion == TargetVersion.Live)
                {
                    dataStoreRepository.DeleteData(dataflow.Dsd,
                        DbTableVersions.GetNewTableVersion((DbTableVersion)dataflow.Dsd.LiveVersion));
                }

                //Log result
                switch (transferParam.ValidationType)
                {
                    case ValidationType.ImportWithFullValidation:
                        Log.Notice(string.Format(
                            LocalizationRepository.GetLocalisedResource(
                                Localization.ResourceId.ObservationsProcessedDetails,
                                transferParam.CultureInfo.TwoLetterISOLanguageName), mergeResult.TotalCount,
                            mergeResult.InsertCount, mergeResult.UpdateCount, mergeResult.DeleteCount));
                        break;
                    case ValidationType.ImportWithBasicValidation:
                        Log.Notice(string.Format(
                            LocalizationRepository.GetLocalisedResource(
                                Localization.ResourceId.ObservationsProcessedNoDetails,
                                transferParam.CultureInfo.TwoLetterISOLanguageName), mergeResult.TotalCount));
                        break;
                    case ValidationType.FullValidationOnly:
                        Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ValidationSucceeded,
                                transferParam.CultureInfo.TwoLetterISOLanguageName));
                        break;
                }

                //Close the transaction 
                if (transferParam.ValidationType == ValidationType.FullValidationOnly)
                {
                    //Close transaction and drop staging tables
                    dataStoreRepository.DropStagingTables(dataflow.Dsd.DbId);
                    mgmtRepository.CloseTransactionItem(transferParam.Id, true, false);

                }
                else{
                    //Close transaction, update ACC and mapping sets
                    transRepository.CloseTransaction(transferParam.Id, dataflow, _mappingStoreDataAccess, targetTableVersion, transferParam.PITRestorationAllowed);
                }


            }
            catch (System.Exception)
            {
                transRepository.CleanUpFailedTransaction(transferParam.Id);
                throw;
            }

            return true;
        }
    }
}