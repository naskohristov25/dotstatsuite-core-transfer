﻿
using System.Diagnostics.CodeAnalysis;

namespace DotStat.Transfer.Exception
{
    public class ConcurrentTransactionException : System.Exception
    {
        [ExcludeFromCodeCoverage]
        public ConcurrentTransactionException()
        {
        }

        [ExcludeFromCodeCoverage]
        public ConcurrentTransactionException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public ConcurrentTransactionException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
