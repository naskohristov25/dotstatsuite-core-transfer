﻿
using System.Diagnostics.CodeAnalysis;

namespace DotStat.Transfer.Exception
{
    public class DataflowAlreadyInitializedException : System.Exception
    {
        [ExcludeFromCodeCoverage]
        public DataflowAlreadyInitializedException()
        {
        }

        [ExcludeFromCodeCoverage]
        public DataflowAlreadyInitializedException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public DataflowAlreadyInitializedException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
