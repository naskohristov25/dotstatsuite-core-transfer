﻿
using System.Diagnostics.CodeAnalysis;

namespace DotStat.Transfer.Exception
{
    public class TransferFailedException : System.Exception
    {
        [ExcludeFromCodeCoverage]
        public TransferFailedException()
        {
        }

        [ExcludeFromCodeCoverage]
        public TransferFailedException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public TransferFailedException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
