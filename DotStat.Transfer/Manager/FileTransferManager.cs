﻿using DotStat.Common.Configuration;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using System;

namespace DotStat.Transfer.Manager
{
    public class FileTransferManager<T> : TransferManager<T> where T : TransferParam
    {
        public FileTransferManager(BaseConfiguration configuration, IProducer<T> observationProducer,
            IConsumer<T> observationConsumer) : base(configuration, observationProducer, observationConsumer)
        {
        }

        public override void Transfer(T transferParam)
        {
            if (transferParam == null)
            {
                throw new ArgumentNullException(nameof(transferParam));
            }

            var destinationDataflow = Producer.GetDataflow(transferParam);

            if (!Consumer.IsAuthorized(transferParam, destinationDataflow))
            {
                throw new TransferUnauthorizedException();
            }

            var transferContent = Producer.Process(transferParam, destinationDataflow);

            if (!HasComponentsToProcess(destinationDataflow, transferContent))
                return;

            if (!Consumer.Save(transferParam, destinationDataflow, transferContent))
            {
                throw new TransferFailedException();
            }

            Log.Notice(
                LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.StreamingFinished,
                    transferParam.CultureInfo.TwoLetterISOLanguageName));
        }
    }
}
