﻿using DotStat.Common.Configuration;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;

namespace DotStat.Transfer.Manager
{
    public class SdmxFileToSqlTransferManager : FileTransferManager<SdmxFileToSqlTransferParam>
    {
        public SdmxFileToSqlTransferManager(BaseConfiguration configuration, IProducer<SdmxFileToSqlTransferParam> observationProducer, 
            IConsumer<SdmxFileToSqlTransferParam> observationConsumer) : base(configuration, observationProducer, observationConsumer)
        {
        }
    }
}
