﻿namespace DotStat.Transfer.Param
{
    public class SdmxFileToSqlTransferParam : TransferParam, ITransferParamWithFilePath
    {
        public string FilePath { get; set; }
    }
}