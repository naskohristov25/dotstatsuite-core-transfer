﻿namespace DotStat.Transfer.Param
{
    public class SdmxMetadataFileToSqlTransferParam : TransferParam, ITransferParamWithFilePath
    {
        public string FilePath { get; set; }
    }
}

