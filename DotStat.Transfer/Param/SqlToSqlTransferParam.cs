﻿using DotStat.Domain;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;

namespace DotStat.Transfer.Param
{
    public interface ISqlTransferParam : ITransferParam
    {
        IDataflowMutableObject SourceDataflow { get;}
        string SourceQuery { get;}

        IDataflowMutableObject DestinationDataflow { get; set; }
    }

    public interface IFromSqlTransferParam : ISqlTransferParam
    {
        TargetVersion SourceVersion { get; set; }
    }

    public class SqlToSqlTransferParam : TransferParam, IFromSqlTransferParam
    {
        public IDataflowMutableObject SourceDataflow { get; set; }
        public IDataflowMutableObject DestinationDataflow { get; set; }
        public string SourceQuery { get; set; }

        public TargetVersion SourceVersion { get; set; }
    }
}
