﻿using System;
using System.Collections.Generic;
using System.Globalization;
using DotStat.Common.Auth;
using DotStat.Common.Configuration.Dto;
using DotStat.Domain;

namespace DotStat.Transfer.Param
{
    public interface ITransferParam
    {
        int Id { get; set; }
        DataspaceInternal SourceDataspace { get; set; }
        DataspaceInternal DestinationDataspace { get; set; }
        DotStatPrincipal Principal { get; set; }
        CultureInfo CultureInfo { get; set; }
        TargetVersion TargetVersion { get; set; }
        DateTime? PITReleaseDate { get; set; }
        Boolean PITRestorationAllowed { get; set; }
        string DataSource { get; set; }
        ValidationType ValidationType { get; set; }
    }

    public interface ITransferParamWithFilePath : ITransferParam
    {
        string FilePath { get; set; }
    }

    public class TransferParam : ITransferParam
    {
        public int Id { get; set; }
        public DataspaceInternal SourceDataspace { get; set; }
        public DataspaceInternal DestinationDataspace { get; set; }
        public DotStatPrincipal Principal { get; set; }
        public CultureInfo CultureInfo { get; set; }
        public TargetVersion TargetVersion { get; set; }
        public DateTime? PITReleaseDate { get; set; }
        public Boolean PITRestorationAllowed { get; set; }
        public string DataSource { get; set; }
        public ValidationType ValidationType { get; set; }
        public IList<string> FilesToDelete { get; set; } = new List<string>();
    }

    /// <summary>
    /// The type of validation to use during request: ImportWithBasicValidation (0), ImportWithFullValidation (1), FullValidationOnly (2).
    /// </summary>
    public enum ValidationType
    {
        /// <summary>
        /// Import/transfer data with full validation
        /// </summary>
        ImportWithBasicValidation = 0,
        /// <summary>
        /// Import/transfer data with full validation
        /// </summary>
        ImportWithFullValidation = 1,
        /// <summary>
        /// Full validation only (No data import/transfer).
        /// </summary>
        FullValidationOnly = 2,
    }

    /// <summary>
    /// The type of validation to use during import/transfer: ImportWithBasicValidation (0), ImportWithFullValidation (1) (default ImportWithBasicValidation (0).
    /// </summary>
    public enum ImportValidationType
    {
        /// <summary>
        /// Import/transfer data with full validation
        /// </summary>
        ImportWithFullValidation = ValidationType.ImportWithFullValidation,
        /// <summary>
        /// Import/transfer data with basic validation
        /// </summary>
        ImportWithBasicValidation = ValidationType.ImportWithBasicValidation,
    }
}