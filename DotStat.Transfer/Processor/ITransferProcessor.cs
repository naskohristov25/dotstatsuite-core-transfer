﻿using DotStat.Domain;

namespace DotStat.Transfer.Processor
{
    public interface ITransferProcessor
    {
        TransferContent Process(Dataflow sourceDataflow, Dataflow destinationDataflow, TransferContent transferContent);
    }
}
