﻿using System.IO;
using System.Xml.Linq;
using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Common.Logger;
using DotStat.MappingStore;
using DotStat.Transfer.Excel.Excel;
using DotStat.Transfer.Excel.Reader;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System.Collections.Generic;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using System.Linq;

namespace DotStat.Transfer.Producer
{
    public class ExcelProducer : IProducer<ExcelToSqlTransferParam>
    {
        private readonly IMappingStoreDataAccess _dataAccess;
        private ExcelDataDescription _excelDataDescription;
        private EPPlusExcelDataSource _excelSource;

        public ExcelProducer(IMappingStoreDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        public bool IsAuthorized(ExcelToSqlTransferParam transferParam, Dataflow dataflow)
        {
            //As the Source is Excel there is no need to check if the User can read Data.
            return true;
        }

        public Dataflow GetDataflow(ExcelToSqlTransferParam transferParam, bool throwErrorIfNotFound=true)
        {
            _excelDataDescription = ExcelDataDescription.Build(
                1,
                string.Empty,
                string.Empty,
                XDocument.Load(new StreamReader(transferParam.EddFilePath)),
                _dataAccess,
                transferParam.DestinationDataspace?.Id);

            var dataFlow = _excelDataDescription.Dataflow;

            if (transferParam.DestinationDataspace != null)
                Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.DataflowLoaded,
                            transferParam.CultureInfo.TwoLetterISOLanguageName),
                        dataFlow.FullId,
                        transferParam.DestinationDataspace.Id));

            return dataFlow;
        }

        public TransferContent Process(ExcelToSqlTransferParam transferParam, Dataflow dataflow)
        {
            if (!IsAuthorized(transferParam, dataflow))
            {
                throw new TransferUnauthorizedException();
            }

            _excelSource = new EPPlusExcelDataSource(transferParam.ExcelFilePath);

            var dimGroupSeriesKeyables = GetDimGroupSeriesFromExcelFile(dataflow);
            var observations = new SWCanonicalReader<IObservation>(_excelDataDescription.GetObservationCellIterator(_excelSource), dataflow).AsIEnumerable();

            var newObservations =new List<IObservation>();
            if (dimGroupSeriesKeyables != null)
            {
                foreach (var currentObservation in observations)
                {
                    var currentKeyableCoordinate = new HashSet<string>(currentObservation.SeriesKey.Key.Select(k => $"{k.Concept}:{k.Code}"));
                    var dimGroupSeriesAttributes = dimGroupSeriesKeyables
                        .Where(dimGroupSerie => dimGroupSerie.Key.IsSubsetOf(currentKeyableCoordinate))
                        .SelectMany(item => item.Value);

                    var allAttributes = new List<IKeyValue>(currentObservation.Attributes);
                    allAttributes.AddRange(dimGroupSeriesAttributes);

                    //TODO: We could use a lighter object (Not IObservation) to improve performance
                    newObservations.Add(new ObservationImpl(currentObservation.SeriesKey, currentObservation.ObsTime, currentObservation.ObservationValue, allAttributes, crossSectionValue:null));
                };
            }
            else
            {
                newObservations = observations.ToList();
            }


            return new TransferContent
            {
                Observations = newObservations,
                DatasetAttributes = _excelDataDescription.DatasetAttributesDescriptor != null
                    ? new SWCanonicalReader<IKeyValue>(_excelDataDescription.GetDatasetAttributesIterator(_excelSource),
                        dataflow).AsIEnumerable()
                    : null,
                ReportedComponents = new ReportedComponents
                {
                    Dimensions = dataflow.Dsd.Dimensions.ToList(),
                    IsPrimaryMeasureReported = true,
                    Attributes = dataflow.Dsd.Attributes.ToList()
                }
            };
        }

        private Dictionary<HashSet<string>, List<IKeyValue>> GetDimGroupSeriesFromExcelFile(Dataflow dataflow)
        {
            var dimGroupSeriesKeyables = new Dictionary<HashSet<string>, List<IKeyValue>>();
            var seriesKeyables = new SWCanonicalReader<IKeyable>(_excelDataDescription.GetDimAttributesIterator(_excelSource), dataflow).AsIEnumerable();
            var enumerator = (seriesKeyables ?? Enumerable.Empty<IKeyable>()).GetEnumerator();

            while (enumerator.MoveNext())
            {
                var currentKeyable = enumerator.Current;
                var keyableCoordinate = new HashSet<string>(currentKeyable.Key.Select(k => $"{k.Concept}:{k.Code}"));
                dimGroupSeriesKeyables.Add(keyableCoordinate, currentKeyable.Attributes.ToList());                
                //TODO joining subsets to supersets might improve performance?
            }
            return dimGroupSeriesKeyables;
        }

        public void Dispose()
        {
            if (_excelSource != null)
            {
                _excelSource.Dispose();
            }
        }
    }
}