﻿using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.Transfer.Param;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.DataParser.Engine.Csv;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Util.Io;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;

namespace DotStat.Transfer.Producer
{
    public abstract class FileProducer<T> : IProducer<T> where T : TransferParam, ITransferParamWithFilePath
    {
        protected readonly ReadableDataLocationFactory _dataLocationFactory;

        private readonly ITempFileManagerBase _tempFileManager;
        private Dataflow _dataFlow;
        private string _timeDimension;

        protected FileProducer(ITempFileManagerBase tempFileManager)
        {
            _tempFileManager = tempFileManager;
            _dataLocationFactory = new ReadableDataLocationFactory();
        }

        public abstract ReportedComponents GetReportedComponents(string filename, Dataflow dataflow);

        public abstract Dataflow GetDataflowFromSdmxFile(T transferParam);

        public bool IsAuthorized(T transferParam, Dataflow dataflow)
        {
            return true;
        }

        public Dataflow GetDataflow(T transferParam, bool throwErrorIfNotFound = true)
        {
            if (_dataFlow != null)
            {
                return _dataFlow;
            }

            GetFileFromSource(transferParam);
            _dataFlow = GetDataflowFromSdmxFile(transferParam);

            if (_dataFlow == null)
            {
                throw new NotImplementedException(LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.NoDataflowReferenceInputDataset,
                    transferParam.CultureInfo.TwoLetterISOLanguageName));
            }

            return _dataFlow;
        }

        public bool IsMetadateFile(T transferParam)
        {
            GetFileFromSource(transferParam);
            return IsCsvDataReaderEngineV2Used(transferParam.FilePath);
        }

        public TransferContent Process(T transferParam, Dataflow dataflow)
        {
            var fileInfo = new FileInfo(transferParam.FilePath);

            if (!fileInfo.Exists)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FileNotFound),
                    transferParam.FilePath)
                );

            return Process(fileInfo.FullName, dataflow);
        }

        private TransferContent Process(string filename, Dataflow dataflow)
        {
            if (dataflow.Dsd.Base.TimeDimension != null)
                _timeDimension = dataflow.Dsd.Base.TimeDimension.Id;

            var datasetAttributes = new List<IKeyValue>();

            return new TransferContent()
            {
                DatasetAttributes = datasetAttributes,
                Observations = ProcessSdmxFile(filename, dataflow, datasetAttributes),
                ReportedComponents = GetReportedComponents(filename, dataflow)
            };
        }

        private IEnumerable<IObservation> ProcessSdmxFile(string filename, Dataflow dataflow, List<IKeyValue> datasetAttributes)
        {
            using var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            using var dataReaderEngine = new DataReaderManager().GetDataReaderEngine(sourceData, dataflow.Dsd.Base, null, dataflow.Dsd.Msd?.Base);

            foreach (var observation in ReadSdmxFile(dataReaderEngine, datasetAttributes))
            {
                yield return observation;
            }
        }

        private IEnumerable<IObservation> ReadSdmxFile(IDataReaderEngine reader, List<IKeyValue> datasetAttributes)
        {
            datasetAttributes.Clear();
            var groupAttributes = new Dictionary<HashSet<string>, List<IKeyValue>>();

            var finishedProcessingGroups = false;
            if (reader.MoveNextDataset())
            {
                datasetAttributes.AddRange(reader.DatasetAttributes);

                while (reader.MoveNextKeyable())
                {
                    //group found after processing series
                    if (finishedProcessingGroups && !reader.CurrentKey.Series)
                        throw new ArgumentException(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .SDMXMLErrorGropsAfterSeries));

                    var currentKeyable = reader.CurrentKey;
                    var currentKeyableCoordinate =
                        new HashSet<string>(currentKeyable.Key.Select(k => $"{k.Concept}:{k.Code}"));

                    //Process group keys
                    if (!reader.CurrentKey.Series)
                    {
                        groupAttributes.Add(currentKeyableCoordinate, currentKeyable.Attributes.ToList());
                        continue;
                    }
                    //Process series keys and observations
                    else
                    {
                        finishedProcessingGroups = true;

                        var dimGroupAttributesWithNoTimeDim = new List<IKeyValue>();
                        if (groupAttributes != null)
                        {
                            dimGroupAttributesWithNoTimeDim = groupAttributes
                                .Where(dimGroupSerie => dimGroupSerie.Key.IsSubsetOf(currentKeyableCoordinate))
                                .SelectMany(item => item.Value).ToList();
                        }

                        if (reader.MoveNextObservation())
                        {
                            do
                            {
                                var currentObservation = reader.CurrentObservation;

                                //Include group attributes referencing the time dimension
                                var dimGroupAttributesWithTimeDim = new List<IKeyValue>();
                                if (!string.IsNullOrEmpty(_timeDimension) && groupAttributes != null)
                                {
                                    var currentObsCoordinate = new HashSet<string>(
                                        currentObservation.SeriesKey.Key.Select(k => $"{k.Concept}:{k.Code}"));
                                    dimGroupAttributesWithTimeDim = groupAttributes
                                        .Where(dimGroupSerie => dimGroupSerie.Key.IsSubsetOf(currentObsCoordinate))
                                        .SelectMany(item => item.Value)
                                        .Where(attribute => attribute.Concept == _timeDimension)
                                        .ToList();
                                }

                                //add obs attributes
                                var allAttributes = new List<IKeyValue>(currentObservation.Attributes);
                                //add series attributes
                                allAttributes.AddRange(currentKeyable.Attributes);
                                //add group attributes with no time dim ref
                                allAttributes.AddRange(dimGroupAttributesWithNoTimeDim);
                                //add group attributes with  time dim ref
                                allAttributes.AddRange(dimGroupAttributesWithTimeDim);

                                //TODO: We could use a lighter object (Not IObservation) to improve performance
                                yield return new ObservationImpl(currentKeyable, currentObservation.ObsTime,
                                    currentObservation.ObservationValue, allAttributes, crossSectionValue: null);
                            } while (reader.MoveNextObservation());
                        }
                    }
                }
            }
        }

        private void GetFileFromSource(T transferParam)
        {
            var fileInfo = new FileInfo(transferParam.FilePath);

            if (!fileInfo.Exists)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FileNotFound),
                    transferParam.FilePath)
                );

            if (transferParam.FilePath.EndsWith(".zip", StringComparison.OrdinalIgnoreCase) ||
                !string.IsNullOrEmpty(transferParam.DataSource) &&
                transferParam.DataSource.EndsWith(".zip", StringComparison.OrdinalIgnoreCase))
            {
                var tempFile = _tempFileManager.GetTempFileName();

                using (var zip = ZipFile.OpenRead(fileInfo.FullName))
                {
                    var entry = zip.Entries.First();
                    entry.ExtractToFile(tempFile, true);
                }

                transferParam.FilesToDelete.Add(tempFile);
                transferParam.FilePath = tempFile;
            }
        }

        private bool IsCsvDataReaderEngineV2Used(string filePath)
        {
            using var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            using var dataReaderEngine = new DataReaderManager().GetDataReaderEngine(sourceData, new InMemoryRetrievalManager());

            return dataReaderEngine is CsvDataReaderEngineV2;
        }

        public void Dispose()
        {
            //Nothing to do, IDataReaderEngine is within a "using" clause
        }
    }
}
