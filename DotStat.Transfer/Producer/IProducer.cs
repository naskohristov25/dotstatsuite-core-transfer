﻿using System;
using DotStat.Domain;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Param;

namespace DotStat.Transfer.Producer
{
    public interface IProducer<in T>: IDataflowManager<T>, IDisposable where T : TransferParam
    {
        TransferContent Process(T transferParam, Dataflow dataflow);

        bool IsAuthorized(T transferParam, Dataflow dataflow);
    }
}