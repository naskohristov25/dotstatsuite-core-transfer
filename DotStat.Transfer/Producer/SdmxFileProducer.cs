﻿using DotStat.Common.Localization;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Param;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.DataParser.Engine.Csv;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using System;
using System.IO;
using System.Linq;

namespace DotStat.Transfer.Producer
{
    public class SdmxFileProducer : FileProducer<SdmxFileToSqlTransferParam>
    {
        private readonly IMappingStoreDataAccess _dataAccess;

        public SdmxFileProducer(IMappingStoreDataAccess dataAccess, ITempFileManagerBase tempFileManager) : base(tempFileManager)
        {
            _dataAccess = dataAccess;
        }

        public override ReportedComponents GetReportedComponents(string filename, Dataflow dataflow)
        {
            using var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            using var dataReaderEngine = new DataReaderManager().GetDataReaderEngine(sourceData, dataflow.Dsd.Base, null);

            var reportedComponents = new ReportedComponents();
            reportedComponents.Dimensions = dataflow.Dsd.Dimensions.ToList();
            if (!(dataReaderEngine is CsvDataReaderEngine))
            {
                reportedComponents.IsPrimaryMeasureReported = true;
                reportedComponents.Attributes = dataflow.Dsd.Attributes.ToList();
                return reportedComponents;
            }

            if (!dataReaderEngine.MoveNextDataset() || !dataReaderEngine.MoveNextKeyable() ||
                !dataReaderEngine.MoveNextObservation()) return reportedComponents;

            var currentObservation = dataReaderEngine.CurrentObservation;
            //Measure column found in csv
            if (currentObservation.ObservationValue != null)
                reportedComponents.IsPrimaryMeasureReported = true;

            //Dataset attributes' columns found in csv
            reportedComponents.Attributes = dataflow.Dsd.Attributes
                .Where(a => dataReaderEngine.DatasetAttributes.Any(ar =>
                    ar.Concept.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase))).ToList();

            //All other attributes' columns found in csv
            var observationAttributes = dataflow.Dsd.Attributes
                .Where(a => currentObservation.Attributes.Any(ar =>
                    ar.Concept.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase)));
            reportedComponents.Attributes.AddRange(observationAttributes);

            return reportedComponents;
        }

        public override Dataflow GetDataflowFromSdmxFile(SdmxFileToSqlTransferParam transferParam)
        {
            try
            {
                using (var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileStream(transferParam.FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite)))
                {
                    using (var dataReaderEngine = new DataReaderManager().GetDataReaderEngine(sourceData, _dataAccess.GetRetrievalManager(transferParam.DestinationDataspace.Id)))
                    {
                        if (dataReaderEngine.MoveNextDataset())
                        {
                            //1. Read sdmx file/source to get the dataflow
                            var structureReference = dataReaderEngine.CurrentDatasetHeader.DataStructureReference.StructureReference;

                            if (structureReference.TargetReference.EnumType == SdmxStructureEnumType.Dataflow)
                            {
                                //2. Check that the dataflow exists in the mapping store database
                                return _dataAccess.GetDataflow(transferParam.DestinationDataspace.Id,
                                    structureReference.MaintainableReference.AgencyId,
                                    structureReference.MaintainableReference.MaintainableId,
                                    structureReference.MaintainableReference.Version);
                            }
                        }
                    }
                }
            }
            catch (SdmxSemmanticException e) when (Equals(e.Code, ExceptionCode.SdmxCsvInvalidDelimiter))
            {
                throw new SdmxSemmanticException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CsvInvalidDelimiter), e);
            }
            catch (SdmxSemmanticException e) when (Equals(e.Code, ExceptionCode.SdmxCsvMissingDimensions))
            {
                if (e.InnerException != null && !string.IsNullOrEmpty(e.InnerException.Message) &&
                    e.InnerException.Message.Contains(';'))
                {
                    var dimensionAndAttributeList = e.InnerException.Message.Split(';');
                    throw new SdmxSemmanticException(
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CsvMissingDimensions),
                            dimensionAndAttributeList[0], dimensionAndAttributeList[1]), e);
                }

                throw;
            }

            return null;
        }
    }
}