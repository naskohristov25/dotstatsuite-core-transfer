﻿using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Param;
using Org.Sdmxsource.Sdmx.DataParser.Engine.Csv;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DotStat.Transfer.Producer
{
    public class SdmxMetadataFileProducer : FileProducer<SdmxMetadataFileToSqlTransferParam>
    {
        private readonly IMappingStoreDataAccess _dataAccess;

        public SdmxMetadataFileProducer(IMappingStoreDataAccess dataAccess, ITempFileManagerBase tempFileManager) : base(tempFileManager)
        {
            _dataAccess = dataAccess;
        }

        public override ReportedComponents GetReportedComponents(string filename, Dataflow dataflow)
        {
            using var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            using var dataReaderEngine = new DataReaderManager().GetDataReaderEngine(sourceData, dataflow.Dsd.Base, null, dataflow.Dsd.Msd?.Base);

            var reportedComponents = new ReportedComponents
            {
                IsMetadataOnly = true,
                Dimensions = dataflow.Dsd.Dimensions.ToList()
            };

            if (!dataReaderEngine.MoveNextDataset() || !dataReaderEngine.MoveNextKeyable() || !dataReaderEngine.MoveNextObservation())
            {
                return reportedComponents;
            }

            var currentObservation = dataReaderEngine.CurrentObservation;

            if (dataflow.Dsd.Msd != null)
            {
                var attributes = dataflow.Dsd.Msd.MetadataAttributes
                    .Where(x => currentObservation.Attributes.Any(ar =>
                        ar.Concept.Equals(x.Value.Id, StringComparison.InvariantCultureIgnoreCase)));

                reportedComponents.MetadataAttributes = new List<MetadataAttribute>();
                reportedComponents.MetadataAttributes.AddRange(attributes.Select(x => new MetadataAttribute(x.Value) {Dsd = dataflow.Dsd}));
            }

            return reportedComponents;
        }

        public override Dataflow GetDataflowFromSdmxFile(SdmxMetadataFileToSqlTransferParam transferParam)
        {
            var sdmxObjectRetrievalManager = _dataAccess.GetRetrievalManager(transferParam.DestinationDataspace.Id);

            using var sourceData = _dataLocationFactory.GetReadableDataLocation(new FileStream(transferParam.FilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite));
            using var dataReaderEngine = new DataReaderManager().GetDataReaderEngine(sourceData, sdmxObjectRetrievalManager);

            if (!dataReaderEngine.MoveNextDataset())
            {
                return null;
            }

            var dataflow = _dataAccess.GetDataflow(
                transferParam.DestinationDataspace.Id,
                dataReaderEngine.Dataflow.AgencyId,
                dataReaderEngine.Dataflow.Id,
                dataReaderEngine.Dataflow.Version);

            dataflow.Dsd.Msd = new Msd(((CsvDataReaderEngineV2)dataReaderEngine).MetadataStructure);

            return dataflow;
        }
    }
}