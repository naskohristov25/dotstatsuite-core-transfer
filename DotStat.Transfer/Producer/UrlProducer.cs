﻿using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Param;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System.Collections.Generic;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Util.Io;
using System.Net;
using DotStat.Common.Localization;
using System;
using System.Linq;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;

namespace DotStat.Transfer.Producer
{
    public class UrlProducer : IProducer<UrlToSqlTransferParam>
    {
        private readonly ReadableDataLocationFactory _dataLocationFactory;
        private readonly IMappingStoreDataAccess _dataAccess;
        private Dataflow _dataFlow;
        private string _timeDimension;

        public UrlProducer(IMappingStoreDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
            _dataLocationFactory = new ReadableDataLocationFactory();
        }

        public bool IsAuthorized(UrlToSqlTransferParam transferParam, Dataflow dataflow)
        {
            return true;
        }

        public Dataflow GetDataflow(UrlToSqlTransferParam transferParam, bool throwErrorIfNotFound=true)
        {
            if (_dataFlow == null)
            {
                _dataFlow = GetDataflowFromSource(transferParam.Url.ToString(), transferParam.DestinationDataspace.Id);
                if (_dataFlow == null)
                {
                    throw new NotImplementedException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoDataflowReferenceInputDataset, transferParam.CultureInfo.TwoLetterISOLanguageName));
                }
            }
            return _dataFlow;
        }

        public TransferContent Process(UrlToSqlTransferParam transferParam, Dataflow dataflow)
        {
            if (dataflow.Dsd.Base.TimeDimension != null)
                _timeDimension = dataflow.Dsd.Base.TimeDimension.Id;

            var datasetAttributes = new List<IKeyValue>();
            return new TransferContent()
            {
                DatasetAttributes = datasetAttributes,
                // TODO Internal dataspace
                Observations = GetObservations(transferParam.Url.ToString(), dataflow.Dsd, datasetAttributes),
                ReportedComponents = new ReportedComponents
                {
                    Dimensions = dataflow.Dsd.Dimensions.ToList(),
                    IsPrimaryMeasureReported = true,
                    Attributes = dataflow.Dsd.Attributes.ToList()
                }
            };  
        }
        
        private IEnumerable<IObservation> GetObservations(string url, Dsd dsd, List<IKeyValue> datasetAttributes)
        {
            using (var client = new WebClient())
            {
                var task = client.OpenReadTaskAsync(url);
                var waiter = task.GetAwaiter();
                var manager = new DataReaderManager();
                    
                using (var sourceData = _dataLocationFactory.GetReadableDataLocation(waiter.GetResult()))
                {
                    using (var dataReaderEngine = manager.GetDataReaderEngine(sourceData, dsd.Base, null))
                    {
                        foreach (var observation in ReadSource(dataReaderEngine, datasetAttributes))
                        {
                            yield return observation;
                        }
                    }
                }
            }
        }

        private IEnumerable<IObservation> ReadSource(IDataReaderEngine reader, List<IKeyValue> datasetAttributes)
        {
            datasetAttributes.Clear();
            var groupAttributes = new Dictionary<HashSet<string>, List<IKeyValue>>();
            var finishedProcessingGroups = false;

            if (reader.MoveNextDataset())
            {
                datasetAttributes.AddRange(reader.DatasetAttributes);

                while (reader.MoveNextKeyable())
                {
                    //group found after processing series
                    if (finishedProcessingGroups && !reader.CurrentKey.Series)
                        throw new ArgumentException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.SDMXMLErrorGropsAfterSeries));

                    var currentKeyable = reader.CurrentKey;
                    var currentKeyableCoordinate = new HashSet<string>(currentKeyable.Key.Select(k => $"{k.Concept}:{k.Code}"));

                    //Process group keys
                    if (!reader.CurrentKey.Series)
                    {
                        groupAttributes.Add(currentKeyableCoordinate, currentKeyable.Attributes.ToList());
                        continue;
                    }
                    //Process series keys and observations
                    else
                    {
                        finishedProcessingGroups = true;

                        var dimGroupAttributesWithNoTimeDim = new List<IKeyValue>();
                        if (groupAttributes != null)
                        {
                            dimGroupAttributesWithNoTimeDim = groupAttributes
                                .Where(dimGroupSerie => dimGroupSerie.Key.IsSubsetOf(currentKeyableCoordinate))
                                .SelectMany(item => item.Value).ToList();
                        }

                        if (reader.MoveNextObservation())
                        {
                            do
                            {
                                var currentObservation = reader.CurrentObservation;
                                //Include group attributes referencing the time dimension
                                var dimGroupAttributesWithTimeDim = new List<IKeyValue>();
                                if (!string.IsNullOrEmpty(_timeDimension) && groupAttributes != null)
                                {
                                    var currentObsCoordinate = new HashSet<string>(currentObservation.SeriesKey.Key.Select(k => $"{k.Concept}:{k.Code}"));
                                    dimGroupAttributesWithTimeDim = groupAttributes
                                    .Where(dimGroupSerie => dimGroupSerie.Key.IsSubsetOf(currentObsCoordinate))
                                    .SelectMany(item => item.Value)
                                    .Where(attribute => attribute.Concept == _timeDimension)
                                    .ToList();
                                }
                                //add obs attributes
                                var allAttributes = new List<IKeyValue>(currentObservation.Attributes);
                                //add series attributes
                                allAttributes.AddRange(currentKeyable.Attributes);
                                //add group attributes with no time dim ref
                                allAttributes.AddRange(dimGroupAttributesWithNoTimeDim);
                                //add group attributes with  time dim ref
                                allAttributes.AddRange(dimGroupAttributesWithTimeDim);

                                //TODO: We could use a lighter object (Not IObservation) to improve performance
                                yield return new ObservationImpl(currentKeyable, currentObservation.ObsTime, currentObservation.ObservationValue, allAttributes);
                            }
                            while (reader.MoveNextObservation());
                        }
                    }
                }
            }
        }

        private Dataflow GetDataflowFromSource(string url, string dataspace)
        {
            using (var client = new WebClient())
            {
                var task = client.OpenReadTaskAsync(url);
                var waiter = task.GetAwaiter();
                var manager = new DataReaderManager();

                using (var sourceData = _dataLocationFactory.GetReadableDataLocation(waiter.GetResult()))
                {
                    using (var dataReaderEngine = manager.GetDataReaderEngine(sourceData,  _dataAccess.GetRetrievalManager(dataspace)))
                    {
                        if (dataReaderEngine.MoveNextDataset())
                        {
                            //1. Read sdmx file/source to get the dataflow
                            var structureReference = dataReaderEngine.CurrentDatasetHeader.DataStructureReference.StructureReference;
                            var structureType = structureReference.TargetReference.EnumType;
                            if (structureType == Org.Sdmxsource.Sdmx.Api.Constants.SdmxStructureEnumType.Dataflow)
                            {
                                //2. Check that the dataflow exists in the mapping store database
                                return _dataAccess.GetDataflow(dataspace, structureReference.MaintainableReference.AgencyId, structureReference.MaintainableReference.MaintainableId, structureReference.MaintainableReference.Version);
                            }
                        }
                    }
                }
            }
            return null;

        }

        public void Dispose()
        {
            //Nothing to do, IDataReaderEngine is within a "using" clause
        }
    }
}