﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace DotStatServices.Transfer.BackgroundJob
{
    /// <summary>
    /// Class to queue background tasks, registered as a singleton in startup
    /// Todo: consider if we need separate queues for import & transfer, if yes this class can be made generic with separate instances for different queues.
    /// </summary>
    public class BackgroundQueue
    {
        private readonly Action<Exception> _onException;
        private readonly ConcurrentQueue<Func<CancellationToken, Task>> Queue = new ConcurrentQueue<Func<CancellationToken, Task>>();
        private int _runningTasks;
        internal readonly int MaxConcurrentCount;
        internal readonly int MillisecondsToWaitBeforePickingUpTask;

        internal int TaskInQueue => Queue.Count;
        internal int RunningTasks => _runningTasks;

        // user for mocking in unit test
        public BackgroundQueue() : this(null, 10, 1000)
        {}

        public BackgroundQueue(Action<Exception> onException, int maxConcurrentCount, int millisecondsToWaitBeforePickingUpTask)
        {
            if (millisecondsToWaitBeforePickingUpTask < 500)
                throw new ArgumentException("< 500 Milliseconds will eat the CPU", nameof(millisecondsToWaitBeforePickingUpTask));

            if (maxConcurrentCount < 1)
                throw new ArgumentException("maxConcurrentCount must be at least 1", nameof(maxConcurrentCount));

            _onException = onException ?? (exception => { });
            MaxConcurrentCount = maxConcurrentCount;
            MillisecondsToWaitBeforePickingUpTask = millisecondsToWaitBeforePickingUpTask;
        }

        public virtual void Enqueue(Func<CancellationToken, Task> task)
        {
            Queue.Enqueue(task);
        }

        internal async Task Dequeue(CancellationToken cancellationToken)
        {
            if (Queue.TryDequeue(out var nextTaskAction))
            {
                Interlocked.Increment(ref _runningTasks);
                try
                {
                    await nextTaskAction(cancellationToken);
                }
                catch (Exception e)
                {
                    _onException(e);
                }
                finally
                {
                    Interlocked.Decrement(ref _runningTasks);
                }
            }

            await Task.CompletedTask;
        }
    }
}
