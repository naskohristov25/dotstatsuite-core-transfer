﻿using System.IO;
using System.Threading.Tasks;
using DotStat.Transfer;
using DotStatServices.Transfer.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.DependencyInjection;

namespace DotStatServices.Transfer.BackgroundJob
{
    public interface ITempFileManager : ITempFileManagerBase
    {
        public Task<FormValueProvider> StreamBodyToDisk(HttpRequest request, IFileParameter parameters);

        public FileStream TryCreateTempFileStream(string prefix = null);

        public bool DeleteTempFile(string file);

        public void RegisterService(IServiceCollection services);

        public void ClearTempFiles();
    }
}
