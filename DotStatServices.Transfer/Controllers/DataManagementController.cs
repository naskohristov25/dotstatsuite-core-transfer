﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Manager;
using DotStat.Domain;
using DotStat.MappingStore;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStat.Db.Repository;
using Estat.Sdmxsource.Extension.Constant;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Services;
using System.Threading.Tasks;
using DotStat.MappingStore.Exception;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// CleanUp controller
    /// </summary>
    [ApiVersion("1.2")]
    public class DataManagementController : ControllerBase
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IAuthConfiguration _authConfiguration;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly BaseConfiguration _configuration;
        private readonly IDbManager _dbManager;
        private readonly CommonManager _commonManager;
        private readonly BackgroundQueue _backgroundQueue;
        private readonly IMailService _mailService;

        /// <summary>
        /// Constructor for this controller filled by DryIoc
        /// </summary>
        public DataManagementController(
            IHttpContextAccessor contextAccessor,
            IAuthConfiguration authConfiguration,
            IAuthorizationManagement authorizationManagement,
            IMappingStoreDataAccess mappingStoreDataAccess, 
            BaseConfiguration configuration,
            IDbManager dbManager,
            CommonManager commonManager,
            IMailService mailService,
            BackgroundQueue backgroundQueue)
        {
            _contextAccessor = contextAccessor;
            _authConfiguration = authConfiguration;
            _authorizationManagement = authorizationManagement;
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _configuration = configuration;
            _dbManager = dbManager;
            _commonManager = commonManager;
            _mailService = mailService;
            _backgroundQueue = backgroundQueue;
        }

        /// <summary>
        /// Initializes database objects of a dataflow in datastore database
        /// </summary>
        /// <param name="dataspace">Dataspace</param>
        /// <param name="dataflow">SDMX id of dataflow in the format of AGENCY_ID:DATAFLOW_ID(VERSION)</param>
        /// <response code="200">Initialization has already been done</response>
        /// <response code="201">Initialization successfully completed</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="401">Not authorized.</response>
        /// <response code="409">Conflict. There is an ongoing transaction targeting the DSD of the dataflow</response>
        [Microsoft.AspNetCore.Mvc.HttpPost]
        [Microsoft.AspNetCore.Mvc.Route("{version:apiVersion}/init/dataflow")]
        public ActionResult<OperationResult> InitDatabaseObjectsOfDataflow(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dataflow)
        {
            var transferParam = new TransferParam()
            {
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                DestinationDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode),
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping),
                TargetVersion = TargetVersion.Live,
                PITReleaseDate = null
            };

            var sqlTransactionRepository = _dbManager.GetTransactionRepository(transferParam.DestinationDataspace.Id);
            var sqlManagementRepository = _dbManager.GetManagementRepository(transferParam.DestinationDataspace.Id);

            transferParam.Id = sqlManagementRepository.GetNextTransactionId();

            LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);
            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

            try
            {
                if (!_commonManager.InitDataDbObjectsOfDataflow(transferParam,
                    dataflow.GetDataflow(_configuration.DefaultLanguageCode), sqlManagementRepository,
                    sqlTransactionRepository))
                {
                    return new ConflictObjectResult(getResult(
                        false,
                        dataflow,
                        Localization.ResourceId.InitDBObjectsOfDataflowConcurrentTransaction,
                        transferParam.Id,
                        dataspace
                    ));
                }

                return Created(String.Empty, getResult(
                    true,
                    dataflow,
                    Localization.ResourceId.InitDBObjectsOfDataflowSuccess,
                    transferParam.Id,
                    dataspace
                ));

            }
            catch (TransferUnauthorizedException)
            {
                return new ForbidResult();
            }
            catch (ExternalDataflowException)
            {
                return new BadRequestObjectResult(getResult(
                    false,
                    dataflow,
                    Localization.ResourceId.InitExternalDataflow,
                    transferParam.Id,
                    dataspace
                ));
            }
            catch (Exception exception)
            {
                Log.Error(exception);
                throw;
            }
        }

        private OperationResult getResult(
            bool isSuccess,
            string dataflow,
            Localization.ResourceId resourceId,
            int transactionId, 
            string dataspaceId)
        {
            return new OperationResult(
                isSuccess,
                string.Format(LocalizationRepository.GetLocalisedResource(resourceId), dataflow)
            )
            {
                Detail = LogHelper.GetRecordedEvents(transactionId, dataspaceId).Select(o=>new
                {
                    Log = o.Level.DisplayName,
                    Message = o.RenderedMessage
                })
            };
        }

        /// <summary>
        /// Create Mappingsets of all dataflows in mappingstore database
        /// </summary>
        /// <param name="dataspace">Dataspace</param>
        /// <response code="200">Successfully completed</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="401">Not authorized.</response>
        [HttpPost]
        [Route("{version:apiVersion}/init/allMappingsets")]
        public ActionResult<OperationResult> InitAllMappingsets(
            [FromForm, Required] string dataspace)
        {
            var destinationDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode);
            var transactionRepository = _dbManager.GetTransactionRepository(destinationDataspace.Id);
            var managementRepository = _dbManager.GetManagementRepository(destinationDataspace.Id);

            var transferParam = new TransferParam
            {
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                DestinationDataspace = destinationDataspace,
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User,
                _authConfiguration.ClaimsMapping),
                Id = managementRepository.GetNextTransactionId()
            };

            return CreateAllMappingsets(transferParam, managementRepository, transactionRepository);
        }


        private ActionResult<OperationResult> CreateAllMappingsets<T>(T transferParam,
            IManagementRepository managementRepository, ITransactionRepository transactionRepository) where T : TransferParam
        {
            try
            {
                transferParam.Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping);
                
                if (!_authorizationManagement.IsAuthorized(transferParam.Principal, PermissionType.AdminRole))
                    throw new TransferUnauthorizedException();

                var transactionResult = new TransactionResult()
                {
                    TransactionType = TransactionType.Fix,
                    TransactionId = transferParam.Id,
                    DataSource = transferParam.DataSource,
                    SourceDataSpaceId = transferParam.SourceDataspace?.Id,
                    DestinationDataSpaceId = transferParam.DestinationDataspace?.Id,
                    Aftefact = "*",
                    User = transferParam.Principal.Email,
                    TransactionStatus = TransactionStatus.Submitted
                };

                LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);
                Log.SetTransactionId(transferParam.Id);
                Log.SetDataspaceId(transferParam.DestinationDataspace?.Id);

                var message = string.Format(
                    LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.SubmissionResult,
                        transferParam.CultureInfo.TwoLetterISOLanguageName),
                    transferParam.Id);

                Log.Notice(message);

                _backgroundQueue.Enqueue(async cancellationToken =>
                {
                    Log.SetTransactionId(transferParam.Id);
                    Log.SetDataspaceId(transferParam.DestinationDataspace.Id);
                    try
                    {
                        transactionResult.TransactionStatus = TransactionStatus.InProgress;

                        //Manage all mappingSets of the dataFlows belonging to the Dsd
                        if (!transactionRepository.InitializeAllMappingSets(transferParam.Id, _mappingStoreDataAccess,
                            transferParam.Principal))
                        {
                            // Failed execution of procedure (e.g. concurrent transaction)
                            transactionResult.TransactionStatus = TransactionStatus.TimedOutAborted;

                            Log.Warn(string.Format(LocalizationRepository.GetLocalisedResource(
                                Localization.ResourceId.CreateAllMappingsetsFailed,
                                transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                        }
                        else
                        {
                            // Completed without failure
                            transactionResult.TransactionStatus = TransactionStatus.Completed;

                            Log.Notice(string.Format(LocalizationRepository.GetLocalisedResource(
                                Localization.ResourceId.CreateAllMappingsetsCompleted,
                                transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                        }
                    }
                    catch (Exception exception)
                    {
                        transactionResult.TransactionStatus = TransactionStatus.TimedOutAborted;

                        Log.Warn(string.Format(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.CreateAllMappingsetsFailed,
                            transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                        
                        Log.Error(exception);

                        transactionRepository.CleanUpFailedTransaction(transferParam.Id);
                    }
                    finally
                    {
                        _mailService.SendMail(
                                transactionResult,
                                transferParam.CultureInfo.TwoLetterISOLanguageName
                            );
                    }

                    await Task.CompletedTask;
                });

                return new OperationResult(true, message);
            }
            catch (Exception exception)
            {
                Log.Error(exception);
                throw;
            }
        }
    }
}
