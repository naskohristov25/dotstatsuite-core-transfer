﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.Common.Logger;
using DotStat.Db.Manager;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Model;
using DotStatServices.Transfer.Services;
using DotStatServices.Transfer.Utilities;
using Estat.Sdmxsource.Extension.Constant;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Org.Sdmxsource.Util.Url;
using static DotStat.Common.Localization.LocalizationRepository;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// Import controller
    /// </summary>
    [ApiVersion("1.2")]
    public class ImportController : ControllerBase
    {
        private readonly IDbManager _dbManager;
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly ITransferManager<ExcelToSqlTransferParam> _excelTransferManager;
        private readonly ITransferManager<SdmxFileToSqlTransferParam> _sdmxFileTransferManager;
        private readonly ITransferManager<SdmxMetadataFileToSqlTransferParam> _sdmxMetadataFileTransferManager;
        private readonly ITransferManager<UrlToSqlTransferParam> _urlTransferManager;
        private readonly IMailService _mailService;
        private readonly BackgroundQueue _backgroundQueue;
        private readonly BaseConfiguration _configuration;
        private readonly IAuthConfiguration _authConfiguration;
        private readonly ITempFileManager _tempFileManager;

        /// <summary>
        /// Constructor for this Controlled filled by DryIoc
        /// </summary>
        public ImportController(
                IDbManager dbManager,
                IHttpContextAccessor contextAccessor,
                IAuthorizationManagement authorizationManagement,
                ITransferManager<ExcelToSqlTransferParam> excelTransferManager, 
                ITransferManager<SdmxFileToSqlTransferParam> sdmxFileTransferManager,
                ITransferManager<SdmxMetadataFileToSqlTransferParam> sdmxMetadataFileTransferManager,
                ITransferManager<UrlToSqlTransferParam> urlTransferManager,
                BaseConfiguration configuration,
                IAuthConfiguration authConfiguration,
                IMailService mailService,
                BackgroundQueue backgroundQueue,
                ITempFileManager tempFileManager
            )
        {
            _dbManager = dbManager;
            _contextAccessor = contextAccessor;
            _authorizationManagement = authorizationManagement;
            _excelTransferManager = excelTransferManager;
            _sdmxFileTransferManager = sdmxFileTransferManager;
            _sdmxMetadataFileTransferManager = sdmxMetadataFileTransferManager;
            _urlTransferManager = urlTransferManager;
            _configuration = configuration;
            _authConfiguration = authConfiguration;
            _mailService = mailService;
            _backgroundQueue = backgroundQueue;
            _tempFileManager = tempFileManager;
        }
        
        /// <summary>
        /// Initiates import of data from EDD + Excel files into provided data space
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Import task successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>        
        [HttpPost]
        [Route("{version:apiVersion}/import/excel")]
        [DisableFormValueModelBinding]
        public async Task<ActionResult<OperationResult>> ImportExcel([FromForm] ExcelImportParameters inputImportParameters)
        {
            var internalImportParameters = new InternalExcelParameters();
           
            var formModel = await _tempFileManager.StreamBodyToDisk(Request, inputImportParameters);
            var bindingSuccessful = await TryUpdateModelAsync(internalImportParameters, "", formModel);
            if (!bindingSuccessful)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
            }

            internalImportParameters.ImportValidationType = internalImportParameters.validationType == ImportValidationType.ImportWithBasicValidation || internalImportParameters.validationType == null
                ? ValidationType.ImportWithBasicValidation
                : ValidationType.ImportWithFullValidation;

            return ImportExcel(internalImportParameters);
        }

        /// <summary>
        /// Validates the data from EDD + Excel files and the stored data in the provided data space
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Validation task successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>        
        [HttpPost]
        [Route("{version:apiVersion}/validate/excel")]
        [DisableFormValueModelBinding]
        public async Task<ActionResult<OperationResult>> ValidateImportExcel([FromForm] BaseExcelParameters inputImportParameters)
        {
            var formModel = await _tempFileManager.StreamBodyToDisk(Request, inputImportParameters);
            var internalImportParameters = new InternalExcelParameters();
            var bindingSuccessful = await TryUpdateModelAsync(internalImportParameters, "", formModel);
            if (!bindingSuccessful)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
            }
            internalImportParameters.ImportValidationType = ValidationType.FullValidationOnly;
            return ImportExcel(internalImportParameters);
        }

        private ActionResult<OperationResult> ImportExcel(InternalExcelParameters internalExcelParameters)
        {
            //Set default values
            internalExcelParameters.targetVersion ??= TargetVersion.Live;
            internalExcelParameters.restorationOptionRequired ??= false;
            if (internalExcelParameters.targetVersion != TargetVersion.PointInTime)
            {
                internalExcelParameters.PITReleaseDate = null;
                internalExcelParameters.restorationOptionRequired = false;
            }

            var language = internalExcelParameters.lang ?? _configuration.DefaultLanguageCode;

            //Validate files
            internalExcelParameters.EddFileLocalPath.TempLocalFileExists("eddFile", language);
            internalExcelParameters.ExcelFileLocalPath.TempLocalFileExists("excelFile", language);

            var transferParam = new ExcelToSqlTransferParam
            {
                FilesToDelete = new[]
                    {internalExcelParameters.EddFileLocalPath, internalExcelParameters.ExcelFileLocalPath}
            };

            try
            {
                var destinationDataspace = internalExcelParameters.dataspace.GetSpaceInternal(_configuration, language);
                var sqlManagementRepository = _dbManager.GetManagementRepository(destinationDataspace.Id);
                
                if (string.IsNullOrEmpty(internalExcelParameters.EddFileLocalPath))
                    throw new ArgumentException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FileAttachmentNotProvided,
                            language), "excelFile"));

                transferParam.DestinationDataspace = destinationDataspace;
                transferParam.EddFilePath = internalExcelParameters.EddFileLocalPath;
                transferParam.ExcelFilePath = internalExcelParameters.ExcelFileLocalPath;
                transferParam.CultureInfo = CultureInfo.GetCultureInfo(language);
                transferParam.TargetVersion = (TargetVersion)internalExcelParameters.targetVersion;
                transferParam.PITReleaseDate = internalExcelParameters.PITReleaseDate.GetPITReleaseDate(language);
                transferParam.PITRestorationAllowed = (bool)internalExcelParameters.restorationOptionRequired;
                transferParam.ValidationType = internalExcelParameters.ImportValidationType;
                transferParam.DataSource = string.Format(
                    GetLocalisedResource(Localization.ResourceId.EmailSummaryDataSourceEddExcel, language),
                    internalExcelParameters.OriginalEddFileName, internalExcelParameters.OriginalExcelFileName);

                return Import(_excelTransferManager, transferParam, sqlManagementRepository);
            }
            catch
            {
                //cleanup temp local files if there are any errors
                foreach (var fileName in transferParam.FilesToDelete)
                    _tempFileManager.DeleteTempFile(fileName);
                throw;
            }
        }

        /// <summary>
        /// Initiates import of data from SDMX file (xml|csv) into provided data space
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Import task successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        [HttpPost]
        [Route("{version:apiVersion}/import/sdmxFile")]
        [DisableFormValueModelBinding]
        public async Task<ActionResult<OperationResult>> ImportSdmxFile([FromForm] SdmxImportParameters inputImportParameters)
        {
            var formModel = await _tempFileManager.StreamBodyToDisk(Request, inputImportParameters);
            var internalImportParameters = new InternalSdmxParameters();
            var bindingSuccessful = await TryUpdateModelAsync(internalImportParameters, "", formModel);
            if (!bindingSuccessful)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
            }

            internalImportParameters.ImportValidationType = internalImportParameters.validationType == ImportValidationType.ImportWithBasicValidation || internalImportParameters.validationType==null
                ? ValidationType.ImportWithBasicValidation
                : ValidationType.ImportWithFullValidation;

            return ImportSdmxFile(internalImportParameters);
        }

        /// <summary>
        /// Validates the data from SDMX file (xml|csv) file and the stored data in the provided data space
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Import task successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        [HttpPost]
        [Route("{version:apiVersion}/validate/sdmxFile")]
        [DisableFormValueModelBinding]
        public async Task<ActionResult<OperationResult>> ValidateImportSdmxFile([FromForm] BaseSdmxParameters inputImportParameters)
        {
            var formModel = await _tempFileManager.StreamBodyToDisk(Request, inputImportParameters);
            var internalImportParameters = new InternalSdmxParameters();
            var bindingSuccessful = await TryUpdateModelAsync(internalImportParameters, "", formModel);
            if (!bindingSuccessful)
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
            }
            internalImportParameters.ImportValidationType = ValidationType.FullValidationOnly;
            return ImportSdmxFile(internalImportParameters);
        }

        private ActionResult<OperationResult> ImportSdmxFile(InternalSdmxParameters internalSdmxParameters)
        {
            //Set default values
            internalSdmxParameters.targetVersion ??= TargetVersion.Live;
            internalSdmxParameters.restorationOptionRequired ??= false;
            if (internalSdmxParameters.targetVersion != TargetVersion.PointInTime)
            {
                internalSdmxParameters.PITReleaseDate = null;
                internalSdmxParameters.restorationOptionRequired = false;
            }

            var language = internalSdmxParameters.lang ?? _configuration.DefaultLanguageCode;
            var filesToDelete = new List<string>();
            SdmxMetadataFileToSqlTransferParam metadataTransferParam = null;
            SdmxFileToSqlTransferParam sdmxFileTransferParam = null;
            
            if (!string.IsNullOrEmpty(internalSdmxParameters.FileLocalPath))
            {
                filesToDelete.Add(internalSdmxParameters.FileLocalPath);
            }

            try
            {
                var destinationDataspace = internalSdmxParameters.dataspace.GetSpaceInternal(_configuration, language);
                var sqlManagementRepository = _dbManager.GetManagementRepository(destinationDataspace.Id);

                ActionResult<OperationResult> requestMsg;

                //Import from sdmx url source
                if (Extensions.IsValidUrl(internalSdmxParameters.filepath))
                {
                    Extensions.IsAccessible(internalSdmxParameters.filepath);

                    var urlTransferParam = new UrlToSqlTransferParam()
                    {
                        DestinationDataspace = destinationDataspace,
                        Url = internalSdmxParameters.filepath.ToUri(),
                        CultureInfo = CultureInfo.GetCultureInfo(language),
                        TargetVersion = (TargetVersion) internalSdmxParameters.targetVersion,
                        PITReleaseDate = internalSdmxParameters.PITReleaseDate.GetPITReleaseDate(language),
                        PITRestorationAllowed = (bool) internalSdmxParameters.restorationOptionRequired,
                        DataSource = internalSdmxParameters.filepath,
                        ValidationType = internalSdmxParameters.ImportValidationType
                    };

                    requestMsg = Import(_urlTransferManager, urlTransferParam, sqlManagementRepository);

                }
                //import from sdmx file
                else
                {
                    metadataTransferParam = new SdmxMetadataFileToSqlTransferParam();
                    SetTransferParamWithFilePath(metadataTransferParam, internalSdmxParameters, destinationDataspace, language);

                    if (((SdmxMetadataFileProducer)_sdmxMetadataFileTransferManager.Producer).IsMetadateFile(metadataTransferParam))
                    {
                        requestMsg = Import(_sdmxMetadataFileTransferManager, metadataTransferParam, sqlManagementRepository);
                    }
                    else
                    {
                        sdmxFileTransferParam = new SdmxFileToSqlTransferParam();
                        SetTransferParamWithFilePath(sdmxFileTransferParam, internalSdmxParameters, destinationDataspace, language);

                        requestMsg = Import(_sdmxFileTransferManager, sdmxFileTransferParam, sqlManagementRepository);
                    }
                }

                return requestMsg;
            }
            catch
            {
                //cleanup temp local files if there are any errors
                if (metadataTransferParam != null)
                {
                    foreach (var fileName in metadataTransferParam.FilesToDelete)
                    {
                        _tempFileManager.DeleteTempFile(fileName);
                    }
                }

                if (sdmxFileTransferParam != null)
                {
                    foreach (var fileName in sdmxFileTransferParam.FilesToDelete)
                    {
                        _tempFileManager.DeleteTempFile(fileName);
                    }
                }

                foreach (var fileName in filesToDelete)
                {
                    _tempFileManager.DeleteTempFile(fileName);
                }

                throw;
            }
        }

        private static void SetTransferParamWithFilePath<T>(T transferParam, InternalSdmxParameters internalSdmxParameters, DataspaceInternal destinationDataspace, string language) 
            where T : ITransferParamWithFilePath
        {
            transferParam.DestinationDataspace = destinationDataspace;
            transferParam.CultureInfo = CultureInfo.GetCultureInfo(language);
            transferParam.ValidationType = internalSdmxParameters.ImportValidationType;
            transferParam.PITReleaseDate = internalSdmxParameters.PITReleaseDate.GetPITReleaseDate(language);

            if (internalSdmxParameters.targetVersion != null)
            {
                transferParam.TargetVersion = (TargetVersion) internalSdmxParameters.targetVersion;
            }

            if (internalSdmxParameters.restorationOptionRequired != null)
            {
                transferParam.PITRestorationAllowed = (bool) internalSdmxParameters.restorationOptionRequired;
            }

            if (string.IsNullOrEmpty(internalSdmxParameters.filepath))
            {
                internalSdmxParameters.FileLocalPath.TempLocalFileExists("SDMX", language);
                transferParam.FilePath = internalSdmxParameters.FileLocalPath;
                transferParam.DataSource = internalSdmxParameters.OriginalFileName;
            }
            else
            {
                transferParam.FilePath = internalSdmxParameters.filepath;
                transferParam.DataSource = internalSdmxParameters.filepath;
            }
        }

        private ActionResult<OperationResult> Import<T>(ITransferManager<T> transferManager, T transferParam, IManagementRepository managementRepository) where T : TransferParam
        {
            try
            {
                transferParam.Id = managementRepository.GetNextTransactionId();
                transferParam.Principal =
                    new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping);

                var dataflow = transferManager.Producer.GetDataflow(transferParam);

                var transactionResult = new TransactionResult()
                {
                    TransactionType = TransactionType.Import,
                    TransactionId = transferParam.Id,
                    DataSource = transferParam.DataSource,
                    SourceDataSpaceId = transferParam.SourceDataspace?.Id,
                    DestinationDataSpaceId = transferParam.DestinationDataspace?.Id,
                    Aftefact = $"{dataflow?.AgencyId}:{dataflow?.Base.Id}({dataflow?.Version})",
                    User = transferParam.Principal.Email,
                    TransactionStatus = TransactionStatus.Submitted
                };

                LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);
                Log.SetTransactionId(transferParam.Id);
                Log.SetDataspaceId(transferParam.DestinationDataspace?.Id);

                var message = string.Format(
                    LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.SubmissionResult,
                        transferParam.CultureInfo.TwoLetterISOLanguageName),
                    transferParam.Id);

                Log.Notice(message);

                if (!IsAuthorized(transferManager, transferParam))
                    return new ForbidResult();

                _backgroundQueue.Enqueue(async cancellationToken =>
                {
                    Log.SetTransactionId(transferParam.Id);
                    Log.SetDataspaceId(transferParam.DestinationDataspace.Id);
                    try
                    {
                        transactionResult.TransactionStatus = TransactionStatus.InProgress;
                        transferManager.Transfer(transferParam);
                        transactionResult.TransactionStatus = TransactionStatus.Completed;
                    }
                    catch (Exception exception)
                    {
                        transactionResult.TransactionStatus = TransactionStatus.TimedOutAborted;
                        Log.Warn(LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.NoObservationsProcessed,
                            transferParam.CultureInfo.TwoLetterISOLanguageName));
                        Log.Error(exception);
                    }
                    finally
                    {
                        _mailService.SendMail(
                            transactionResult,
                            transferParam.CultureInfo.TwoLetterISOLanguageName
                        );

                        //cleanup temp local files if when the import has completed
                        foreach (var fileName in transferParam.FilesToDelete)
                            _tempFileManager.DeleteTempFile(fileName);
                    }

                    await Task.CompletedTask;
                });

                return new OperationResult(true, message);
            }
            catch (Exception exception)
            {
                Log.Error(exception);

                throw;
            }
        }

        private bool IsAuthorized<T>(ITransferManager<T> transferManager, T transferParam) where T : TransferParam
        {
            var dataflow = transferManager.Producer.GetDataflow(transferParam);

            return _authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.DestinationDataspace.Id,
                dataflow.AgencyId,
                dataflow.Base.Id,
                dataflow.Version.ToString(),
                PermissionType.CanImportData
            );
        }
    }
}
