﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Threading.Tasks;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Manager;
using DotStat.Domain;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static DotStat.Common.Localization.LocalizationRepository;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// Transfer controller
    /// </summary>
    [ApiVersion("1.2")]
    public class TransferController : ControllerBase
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IDbManager _dbManager;
        private readonly ITransferManager<SqlToSqlTransferParam> _transferMngrSql;
        private readonly BaseConfiguration _configuration;
        private IAuthConfiguration _authConfiguration;
        private readonly IMailService _mailService;
        private readonly BackgroundQueue _backgroundQueue;

        /// <summary>
        /// Constructor for this Controlled filled by DryIoc
        /// </summary>
        public TransferController(
            IHttpContextAccessor contextAccessor,
            IDbManager dbManager,
            ITransferManager<SqlToSqlTransferParam> transferMngrSql,
            BaseConfiguration configuration,
            IAuthConfiguration authConfiguration,
            IMailService mailService,
            BackgroundQueue backgroundQueue
        )
        {
            _contextAccessor = contextAccessor;
            _dbManager = dbManager;
            _transferMngrSql = transferMngrSql;
            _configuration = configuration;
            _authConfiguration = authConfiguration;
            _mailService = mailService;
            _backgroundQueue = backgroundQueue;
        }

        /// <summary>
        /// Initiates transfer of data between provided data spaces
        /// </summary>
        /// <param name="sourceDataspace">Source dataspace</param>
        /// <param name="sourceDataflow">Source AGENCYID:DATAFLOWID(VERSION)</param>
        /// <param name="sourceQuery">Source query (e.g: D.NOK.EUR.SP00.A)</param>
        /// <param name="destinationDataspace">Destination dataspace</param>
        /// <param name="destinationDataflow">Destination AGENCYID:DATAFLOWID(VERSION)</param>
        /// <param name="sourceVersion">Source table version LIVE or PIT</param>
        /// <param name="targetVersion">Target table version LIVE or PIT</param>
        /// <param name="lang">Language code</param>
        /// <param name="PITReleaseDate">Point in time release date (dd-MM-yyyy HH:mm:ss)</param>
        /// <param name="restorationOptionRequired">Keep current LIVE version for restoration after PIT release</param>
        /// <param name="validationType">The type of validation to use during import (default ImportWithBasicValidation)</param>
        /// <returns></returns>
        /// <response code="200">Transfer task successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>    
        [HttpPost]
        [Route("{version:apiVersion}/transfer/dataflow")]
        public ActionResult<OperationResult> TransferDataflow(
            [FromForm, Required] string sourceDataspace,
            [FromForm, Required] string sourceDataflow,
            [FromForm] string sourceQuery,
            [FromForm, Required] string destinationDataspace,
            [FromForm, Required] string destinationDataflow,
            [FromForm] string lang,
            [FromForm] TargetVersion? sourceVersion,
            [FromForm] TargetVersion? targetVersion,
            [FromForm] string PITReleaseDate,
            [FromForm] Boolean restorationOptionRequired,
            [FromForm] ImportValidationType? validationType
        )
        {
            if (validationType == null)
                validationType = ImportValidationType.ImportWithBasicValidation;

            var importValidationType = validationType == ImportValidationType.ImportWithBasicValidation
                ? ValidationType.ImportWithBasicValidation
                : ValidationType.ImportWithFullValidation;

            return TransferDataflow(sourceDataspace, sourceDataflow, sourceQuery, destinationDataspace, destinationDataflow,
                lang, targetVersion, sourceVersion, PITReleaseDate, restorationOptionRequired, importValidationType);

        }

        /// <summary>
        /// Validates a data transfer between provided data spaces
        /// </summary>
        /// <param name="sourceDataspace">Source dataspace</param>
        /// <param name="sourceDataflow">Source AGENCYID:DATAFLOWID(VERSION)</param>
        /// <param name="sourceQuery">Source query (e.g: D.NOK.EUR.SP00.A)</param>
        /// <param name="destinationDataspace">Destination dataspace</param>
        /// <param name="destinationDataflow">Destination AGENCYID:DATAFLOWID(VERSION)</param>
        /// <param name="sourceVersion">Source table version LIVE or PIT</param>
        /// <param name="targetVersion">Target table version LIVE or PIT</param>
        /// <param name="lang">Language code</param>
        /// <returns></returns>
        /// <response code="200">The transfer validation task successfully submitted</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>    
        [HttpPost]
        [Route("{version:apiVersion}/validate/transferDataflow")]
        public ActionResult<OperationResult> ValidateTransferDataflow(
            [FromForm, Required] string sourceDataspace,
            [FromForm, Required] string sourceDataflow,
            [FromForm] string sourceQuery,
            [FromForm, Required] string destinationDataspace,
            [FromForm, Required] string destinationDataflow,
            [FromForm] string lang,
            [FromForm] TargetVersion? sourceVersion,
            [FromForm] TargetVersion? targetVersion
        )
        {
            return TransferDataflow(sourceDataspace, sourceDataflow, sourceQuery, destinationDataspace, destinationDataflow,
                lang, targetVersion, sourceVersion, null, false, ValidationType.FullValidationOnly);

        }
        public ActionResult<OperationResult> TransferDataflow(string sourceDataspace, string sourceDataflow,
            string sourceQuery, string destinationDataspace, string destinationDataflow, string lang,
            TargetVersion? targetVersion, TargetVersion? sourceVersion, string PITReleaseDate,
            Boolean restorationOptionRequired, ValidationType validationType)
        {

            //set LIVE version as default
            if (targetVersion == null)
                targetVersion = TargetVersion.Live;

            if (targetVersion != TargetVersion.PointInTime)
            {
                PITReleaseDate = null;
                restorationOptionRequired = false;
            }

            // Set sourceVersion to Live by default
            if (sourceVersion == null)
                sourceVersion = TargetVersion.Live;

            var language = lang ?? _configuration.DefaultLanguageCode;
            var sourceSpace = sourceDataspace.GetSpaceInternal(_configuration, language);
            var destinationSpace = destinationDataspace.GetSpaceInternal(_configuration, language);

            var sqlManagementRepository = _dbManager.GetManagementRepository(destinationSpace.Id);
            var transactionId = sqlManagementRepository.GetNextTransactionId();

            var transferParam = new SqlToSqlTransferParam()
            {
                Id = transactionId,
                SourceDataspace = sourceSpace,
                SourceDataflow = sourceDataflow.GetDataflow(language),
                DestinationDataspace = destinationSpace,
                DestinationDataflow = destinationDataflow.GetDataflow(language),
                SourceQuery = sourceQuery,
                CultureInfo = CultureInfo.GetCultureInfo(lang ?? _configuration.DefaultLanguageCode),
                TargetVersion = (TargetVersion)targetVersion,
                SourceVersion = (TargetVersion)sourceVersion,
                PITReleaseDate = PITReleaseDate.GetPITReleaseDate(language),
                PITRestorationAllowed = restorationOptionRequired,
                ValidationType = validationType
            };

            return DoTransfer(transferParam);

        }
        private ActionResult<OperationResult> DoTransfer(SqlToSqlTransferParam transferParam)
        {
            transferParam.Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping);

            LogHelper.RecordNewTransaction(transferParam.Id, transferParam.DestinationDataspace);
            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

            // ------------------------------------------------

            var dataflow = ((IDataflowManager<SqlToSqlTransferParam>)_transferMngrSql.Consumer).GetDataflow(transferParam);

            if (dataflow == null)
            {
                var template = GetLocalisedResource(Localization.ResourceId.DataflowNotFoundInDataspace, transferParam.CultureInfo.TwoLetterISOLanguageName);

                var errorMessage = string.Format(
                    template,
                    transferParam.DestinationDataflow.AgencyId,
                    transferParam.DestinationDataflow.Id,
                    transferParam.DestinationDataflow.Version,
                    transferParam.DestinationDataspace.Id
                );

                return new BadRequestObjectResult(new OperationResult(false, errorMessage));
            }

            if (!_transferMngrSql.Consumer.IsAuthorized(transferParam, dataflow))
            {
                var template = GetLocalisedResource(Localization.ResourceId.UnauthorizedImport, transferParam.CultureInfo.TwoLetterISOLanguageName);
                var errorMessage = string.Format(
                    template,
                    transferParam.DestinationDataflow.FullId(),
                    transferParam.DestinationDataspace.Id
                );

                return StatusCode(StatusCodes.Status403Forbidden, new OperationResult(false, errorMessage));
            }

            // ------------------------------------------------

            _backgroundQueue.Enqueue(async cancellationToken =>
            {
                Log.SetTransactionId(transferParam.Id);
                Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

                //Set data source information for email message
                var dataSource = string.IsNullOrEmpty(transferParam.SourceQuery) ?
                    GetLocalisedResource(
                        Localization.ResourceId.EmailSummaryDataSourceTransactionNoQuery,
                        transferParam.CultureInfo.TwoLetterISOLanguageName)
                    : string.Format(
                        format: GetLocalisedResource(
                            Localization.ResourceId.EmailSummaryDataSourceTransactionQuery,
                            transferParam.CultureInfo.TwoLetterISOLanguageName), arg0: transferParam.SourceQuery);

                var transactionResult = new TransactionResult()
                {
                    TransactionType = TransactionType.Transfer,
                    TransactionId = transferParam.Id,
                    DataSource = dataSource,
                    SourceDataSpaceId = transferParam.SourceDataspace?.Id,
                    DestinationDataSpaceId = transferParam.DestinationDataspace?.Id,
                    Aftefact = transferParam.DestinationDataflow.FullId(),
                    User = transferParam.Principal.Email,
                    TransactionStatus = TransactionStatus.TimedOutAborted
                };

                try
                {
                    _transferMngrSql.Transfer(transferParam);
                    transactionResult.TransactionStatus = TransactionStatus.Completed;
                }
                catch (Exception exception)
                {
                    Log.Warn(GetLocalisedResource(
                        Localization.ResourceId.NoObservationsProcessed,
                        transferParam.CultureInfo.TwoLetterISOLanguageName)
                    );
                    Log.Error(exception);
                }
                finally
                {
                    _mailService.SendMail(
                        transactionResult,
                        transferParam.CultureInfo.TwoLetterISOLanguageName
                    );
                }

                await Task.CompletedTask;
            });

            var message = string.Format(
                GetLocalisedResource(
                    Localization.ResourceId.SubmissionResult,
                    transferParam.CultureInfo.TwoLetterISOLanguageName),
                transferParam.Id);

            Log.Notice(message);

            return new OperationResult(true, message);
        }


    }
}