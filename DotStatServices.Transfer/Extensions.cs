using DotStat.Common.Configuration.Dto;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using DotStat.Domain;
using log4net.Core;
using DotStat.Db.Dto;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Net.Http.Headers;

namespace DotStatServices.Transfer
{
    internal static class Extensions
    {
        public static readonly HashSet<string> AllowedImportSchemes = new HashSet<string>()
        {
            Uri.UriSchemeHttp,
            Uri.UriSchemeHttps
        };

        public static DataspaceInternal GetSpaceInternal(this string space, IDataspaceConfiguration configuration, string lang, bool mandatory = true)
        {
            if (string.IsNullOrEmpty(space) && mandatory)
                throw new ArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataSpaceNotProvided, lang));

            var dataspace = configuration
                                .SpacesInternal
                                .FirstOrDefault(x => x.Id.Equals(space, StringComparison.InvariantCultureIgnoreCase));

            if (dataspace == null && mandatory)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataSpaceNotFound, lang),
                    space));

            return dataspace;
        }

        public static bool TempLocalFileExists(this string fileLocalPath, string fileName, string lang)
        {
            if(string.IsNullOrEmpty(fileLocalPath) || !File.Exists(fileLocalPath))
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FileAttachmentNotProvided,
                        lang), fileName));
            return true;
        }

        public static bool IsValidExtension(this string fileName, string fieldName, string lang, params string[] extensions)
        {
            if (string.IsNullOrEmpty(fileName) || !extensions.Any(ext => fileName.EndsWith(ext, StringComparison.OrdinalIgnoreCase)))
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NotValidFileExtension, lang),
                    fieldName, fileName, string.Join(",", extensions)));
            return true;
        }
        public static bool IsValidContentType(this string contentType, string fieldName, string lang, params string[] validTypes)
        {
            if (validTypes == null)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ContentTypesUndefined, lang),
                    fieldName));

            if (string.IsNullOrEmpty(contentType) || !validTypes.Any(type => contentType.Equals(type, StringComparison.InvariantCultureIgnoreCase)))
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NotValidContentType, lang),
                    fieldName, string.Join(",", validTypes), fieldName));

            return true;
        }

        public static Encoding GetEncoding(this MultipartSection section)
        {
            var hasMediaTypeHeader = MediaTypeHeaderValue.TryParse(section.ContentType, out var mediaType);
            // UTF-7 is insecure and should not be honored. UTF-8 will succeed in 
            // most cases.
            if (!hasMediaTypeHeader || Encoding.UTF7.Equals(mediaType.Encoding))
            {
                return Encoding.UTF8;
            }
            return mediaType.Encoding;
        }

        public static DataflowMutableCore GetDataflow(this string dataflow, string lang)
        {
            if(string.IsNullOrEmpty(dataflow))
                throw new ArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataFlowNotProvided, lang));

            var elements = dataflow.Split(new[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (elements.Length != 3)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongDataFlowFormat, lang),
                    dataflow));

            return new DataflowMutableCore()
            {
                AgencyId = elements[0],
                Id = elements[1],
                Version = elements[2]
            };
        }

        public static string FullId(this IDataflowMutableObject df)
        {
            if(df == null)
                throw new ArgumentException(nameof(df));

            return $"{df.AgencyId}:{df.Id}({df.Version})";
        }

        public static ArtefactItem GetArtefact(this string artefact, string lang)
        {
            if (string.IsNullOrEmpty(artefact))
                throw new ArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataFlowNotProvided, lang));

            var elements = artefact.Split(new[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (elements.Length != 3)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongDataFlowFormat, lang),
                    artefact));

            return new ArtefactItem()
            {
                Agency = elements[0],
                Id = elements[1],
                Version = elements[2]
            };
        }

        public static DataStructureMutableCore GetDsd(this string dsd, string lang)
        {
            if (string.IsNullOrEmpty(dsd))
                throw new ArgumentException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DSDNotProvided, lang));

            var elements = dsd.Split(new[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (elements.Length != 3)
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongDSDFormat, lang),
                    dsd));

            return new DataStructureMutableCore()
            {
                AgencyId = elements[0],
                Id = elements[1],
                Version = elements[2]
            };
        }

        // ReSharper disable once InconsistentNaming
        public static DateTime? GetPITReleaseDate(this string dateTime, string lang)
        {
            if (string.IsNullOrEmpty(dateTime))
                return null;

            // DateTime does not support 24th hour in accordance with ISO ISO 8601-1:2019: it is explicitly disallowed by the 2019 revision.
            const string validationExpression =
                @"^([1-9][0-9]{3,})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9])(\.[0-9]{1,3})?(Z|(\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$";

            if (Regex.IsMatch(dateTime, validationExpression) && DateTime.TryParse(dateTime, out var dateTimeValue))
            {
                return dateTimeValue; // Return the local (server) date time value
            }

            throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongPITReleaseDateFormat, lang),
                    dateTime, "YYYY-MM-DDThh:mm:ss.sTZD"));
        }

        public static bool IsValidUrl(string url)
        {
            return Uri.TryCreate(url, UriKind.Absolute, out var uriResult) && AllowedImportSchemes.Contains(uriResult.Scheme);
        }

        public static bool IsAccessible(string externalSdmxSourceUrl)
        {
            var request = WebRequest.Create(externalSdmxSourceUrl);
            //Ideally a request to get only the headers (request.Method = WebRequestMethods.Http.Head;) should be made, but some nsiws might not support it
            //As a workaround the contentLength is set to 0, which will return only the headers.
            request.ContentLength = 0;

            try
            {
                using (var response = request.GetResponse() as HttpWebResponse)
                {
                    return response != null && response.StatusCode == HttpStatusCode.OK;
                }
            }
            catch (Exception)
            {
                throw new ArgumentException(message: string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.SdmxSourceHttpRequestException),
                    externalSdmxSourceUrl));
            }

        }

        public static TransactionStatus GetTransactionStatus(Transaction transaction, int transactionTimeOut)
        {
            return transaction.Successful switch
            {
                //Transaction in progress
                null when (DateTime.Now - transaction.ExecutionStart).TotalMinutes < transactionTimeOut =>
                    TransactionStatus.InProgress,
                //Transaction might have timedOut 
                null when (DateTime.Now - transaction.ExecutionStart).TotalMinutes >= transactionTimeOut =>
                    TransactionStatus.TimedOutAborted,
                //Transaction timedOut
                false when transaction.TimedOut == true ||
                    (transaction.ExecutionEnd - transaction.ExecutionStart).Value.TotalMinutes >= transactionTimeOut =>
                    TransactionStatus.TimedOutAborted,
                //Transaction failed
                false => TransactionStatus.Completed,
                //Completed 
                _ => TransactionStatus.Completed
            };
        }

        public static string GetTransactionOutcome(TransactionStatus status, TransactionLog[] transactionLogs)
        {
            if (status == TransactionStatus.Submitted || status == TransactionStatus.InProgress
                || !transactionLogs.Any())//not yet finish processing or no logs
                return TransactionOutcome.None.ToString();
            else if (status == TransactionStatus.TimedOutAborted)
                return TransactionOutcome.Error.ToString();
            else if (transactionLogs.Any(l => l.Level == Level.Error.ToString()))//at least one error
                return TransactionOutcome.Error.ToString();
            else if (transactionLogs.Any(l => l.Level == Level.Warn.ToString()))//at least one warning
                return TransactionOutcome.Warning.ToString();
            else //Processing finished with no errors no warnings
                return TransactionOutcome.Success.ToString();
        }

        internal static DateTime GetSubmissionTime(Transaction transaction, TransactionLog[] transactionLogs)
        {
            var submissionTime = transaction.ExecutionStart;
            var submissionTimeLoggedText = string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.SubmissionResult),
                transaction.TransactionId);

            var loggedSubmissionTime =  transactionLogs.Where(log => log.Message == submissionTimeLoggedText)
                    .Select(log => log.Date).FirstOrDefault();
            if (loggedSubmissionTime != null)
                    submissionTime = (DateTime)loggedSubmissionTime;
 
            return submissionTime;
        }

        public static bool IsValidEmail(this string email, bool throwError= false)
        {
            try
            {
                return new System.Net.Mail.MailAddress(email).Address == email;
            }
            catch
            {
                return throwError
                    ? throw new ArgumentException(
                        message: LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.EmailParameterError))
                    : false;
            }
        }

        public static DateTime? GetDateTime(this string dateTime, string parameterName, string lang)
        {
            if (string.IsNullOrEmpty(dateTime))
                return null;
            const string format = "dd-MM-yyyy HH:mm:ss";
            try
            {
                return DateTime.ParseExact(dateTime, format, CultureInfo.InvariantCulture);
            }
            catch (FormatException)
            {
                throw new ArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongFormat, lang),
                    parameterName, dateTime, format));
            }
        }
    }
}
