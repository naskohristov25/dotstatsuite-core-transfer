﻿using System.ComponentModel.DataAnnotations;
using DotStat.Common.Configuration;
using DotStat.Domain;
using DotStat.Transfer.Param;

namespace DotStatServices.Transfer.Model
{
    public interface IFileParameter
    {
        public string GetFileFieldName(string fieldName);
        public string GetPathFieldName(string fieldName);
        public string[] GetFieldAllowedExtensions(BaseConfiguration configuration, string fieldName);
        public string[] GetFieldAllowedContentTypes(BaseConfiguration configuration, string fieldName);
    }

    public class BaseSdmxParameters : IFileParameter
    {
        /// <summary>
        /// Dataspace
        /// </summary>
        [Display(Order = 1)]
        [Required]
        public string dataspace { get; set; } = "default";

        /// <summary>
        /// AGENCYID:DATAFLOWID(VERSION)
        /// </summary>
        [Display(Order = 2)]
        public string dataflow { get; set; }

        /// <summary>
        /// Language code
        /// </summary>
        [Display(Order = 3)]
        public string lang { get; set; }

        /// <summary>
        /// Path of an SDMX CSV / XML file
        /// </summary>
        [Display(Order = 4)]
        public string filepath { get; set; }

        /// <summary>
        /// SDMX CSV / XML file
        /// </summary>
        [Display(Order = 5)]
        public byte[] file { get; set; }

        /// <summary>
        /// Target table version LIVE (default) or PIT
        /// </summary>
        [Display(Order = 6)]
        public TargetVersion? targetVersion { get; set; } = TargetVersion.Live;

        public string GetFileFieldName(string fieldName)
        {
            return fieldName == "file" ? "OriginalFileName" : string.Empty;
        }

        public string GetPathFieldName(string fieldName)
        {
            return fieldName == "file" ? "FileLocalPath" : string.Empty;
        }

        public string[] GetFieldAllowedExtensions(BaseConfiguration configuration, string fieldName)
        {
            return fieldName == "file" ? configuration.SDMXFileAllowedExtensions : null;
        }

        public string[] GetFieldAllowedContentTypes(BaseConfiguration configuration, string fieldName)
        {
            return fieldName == "file" ? configuration.SDMXFileAllowedContentTypes : null;
        }
    }

    public class SdmxImportParameters : BaseSdmxParameters
    {
        /// <summary>
        /// Point in time release date and time (YYYY-MM-DDThh:mm:ss.sTZD), e.g. 2022-06-04T10:16:01, 2022-06-04T08:16:01Z, 2022-06-04T10:16:01+02:00, 2022-06-04T10:16:01.021+02:00
        /// </summary>
        public string PITReleaseDate { get; set; }

        /// <summary>
        /// Keep current LIVE version for restoration after PIT release
        /// </summary>
        public bool? restorationOptionRequired { get; set; } = false;

        /// <summary>
        /// The type of validation to use during import (default 0; ImportWithBasicValidation)
        /// </summary>
        public ImportValidationType? validationType { get; set; } = ImportValidationType.ImportWithBasicValidation;
    }

    public class InternalSdmxParameters : SdmxImportParameters
    {
        public string OriginalFileName { get; set; }
        public string FileLocalPath { get; set; }
        public ValidationType ImportValidationType { get; set; }
    }

    public class BaseExcelParameters : IFileParameter
    {
        /// <summary>
        /// Dataspace
        /// </summary>
        [Display(Order = 1)]
        [Required]
        public string dataspace { get; set; } = "default";

        /// <summary>
        /// Language code
        /// </summary>
        [Display(Order = 2)]
        public string lang { get; set; }

        /// <summary>
        /// Edd file
        /// </summary>
        [Display(Order = 3)]
        [Required]
        public byte[] eddFile { get; set; } = {0x00};

        /// <summary>
        /// Excel file
        /// </summary>
        [Display(Order = 4)]
        [Required]
        public byte[] excelFile { get; set; } = {0x00};

        /// <summary>
        /// Target table version LIVE (default) or PIT
        /// </summary>
        [Display(Order = 5)]
        public TargetVersion? targetVersion { get; set; } = TargetVersion.Live;

        public string GetFileFieldName(string fieldName)
        {
            return fieldName switch
            {
                "eddFile" => "OriginalEddFileName",
                "excelFile" => "OriginalExcelFileName",
                _ => null
            };
        }

        public string GetPathFieldName(string fieldName)
        {
            return fieldName switch
            {
                "eddFile" => "EddFileLocalPath",
                "excelFile" => "ExcelFileLocalPath",
                _ => null
            };
        }

        public string[] GetFieldAllowedExtensions(BaseConfiguration configuration, string fieldName)
        {
            return fieldName switch
            {
                "eddFile" => configuration.EddFileAllowedExtensions,
                "excelFile" => configuration.ExcelFileAllowedExtensions,
                _ => null
            };
        }

        public string[] GetFieldAllowedContentTypes(BaseConfiguration configuration, string fieldName)
        {
            return fieldName switch
            {
                "eddFile" => configuration.EddFileAllowedContentTypes,
                "excelFile" => configuration.ExcelFileAllowedContentTypes,
                _ => null
            };
        }
    }

    public class ExcelImportParameters : BaseExcelParameters
    {
        /// <summary>
        /// Point in time release date and time (YYYY-MM-DDThh:mm:ss.sTZD), e.g. 2022-06-04T10:16:01, 2022-06-04T08:16:01Z, 2022-06-04T10:16:01+02:00, 2022-06-04T10:16:01.021+02:00
        /// </summary>
        public string PITReleaseDate { get; set; }

        /// <summary>
        /// Keep current LIVE version for restoration after PIT release
        /// </summary>
        public bool? restorationOptionRequired { get; set; } = false;

        /// <summary>
        /// The type of validation to use during import (default 0; ImportWithBasicValidation)
        /// </summary>
        public ImportValidationType? validationType { get; set; } = ImportValidationType.ImportWithBasicValidation;
    }

    public class InternalExcelParameters : ExcelImportParameters
    {
        public string OriginalEddFileName { get; set; }
        public string EddFileLocalPath { get; set; }
        public string OriginalExcelFileName { get; set; }
        public string ExcelFileLocalPath { get; set; }
        public ValidationType ImportValidationType { get; set; }
    }
}
