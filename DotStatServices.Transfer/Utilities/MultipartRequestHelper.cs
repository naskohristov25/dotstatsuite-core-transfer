﻿using System;
using System.IO;
using DotStat.Common.Localization;
using Microsoft.Net.Http.Headers;

namespace DotStatServices.Transfer.Utilities
{
    public static class MultipartRequestHelper
    {
        public static string GetBoundary(MediaTypeHeaderValue contentType, int lengthLimit, string lang)
        {
            var boundary = HeaderUtilities.RemoveQuotes(contentType.Boundary).Value; 
            if (string.IsNullOrWhiteSpace(boundary))
            {
                throw new InvalidDataException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MissingContentTypeBoundary, lang));
            }

            if (boundary.Length > lengthLimit)
            {
                throw new InvalidDataException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.BoundaryLengthLimit, lang), lengthLimit));
            }

            return boundary;
        }

        public static bool IsMultipartContentType(string contentType)
        {
            return !string.IsNullOrEmpty(contentType)
                    && contentType.IndexOf("multipart/", StringComparison.OrdinalIgnoreCase) >= 0;
        }

        public static bool HasFormDataContentDisposition(ContentDispositionHeaderValue contentDisposition)
        {
            return contentDisposition != null
                    && contentDisposition.DispositionType.Equals("form-data")
                    && string.IsNullOrEmpty(contentDisposition.FileName.Value)
                    && string.IsNullOrEmpty(contentDisposition.FileNameStar.Value); 
        }

        public static bool HasFileContentDisposition(ContentDispositionHeaderValue contentDisposition)
        {
            return contentDisposition != null
                    && contentDisposition.DispositionType.Equals("form-data")
                    && (!string.IsNullOrEmpty(contentDisposition.FileName.Value) 
                        || !string.IsNullOrEmpty(contentDisposition.FileNameStar.Value)); 
        }
    }
}
