﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace DotStatServices.Transfer.Utilities
{
    public class SwaggerFileOperationFilter : IOperationFilter
    {
        [ExcludeFromCodeCoverage]
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            const string fileUploadMime = "multipart/form-data";
            if (operation.RequestBody == null || !operation.RequestBody.Content.Any(x => x.Key.Equals(fileUploadMime, StringComparison.InvariantCultureIgnoreCase)))
                return;

            foreach (var property in operation.RequestBody.Content[fileUploadMime].Schema.Properties
                .Where(p => p.Value.Format=="byte"))
            {
                property.Value.Format = "binary";
            }
        }
    }
}
