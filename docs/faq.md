## How to?
### How to add an extra dataspace?
1. Install a new **DotStatSuiteCore Structure Database** 
2. Install a new **DotStatSuiteCore Data Database**  [See steps](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/blob/master/docs/installation/CodeBaseApproach.md)
3. **Modify the configuration** `/config/dataspaces.private.json`
	*In the array **"spacesInternal"**, add the following json snippet and modify the values accordingly [**See details**](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/blob/master/README.md#configuration):
```json
    {
      "Id": "disseminate",
      "DotStatSuiteCoreStructDbConnectionString": "Data Source=localhost;Initial Catalog=STRUCTURE_DB;User ID=USERNAME;Password=XXXXXX",
      "DotStatSuiteCoreDataDbConnectionString": "Data Source=localhost;Initial Catalog=DATA_DB;User ID=USERNAME;Password=XXXXXX",
      "DataImportTimeOutInMinutes": 60,
      "AuthEndpoint": "",
      "DatabaseCommandTimeoutInSec": 360
    }
```
4. The final configuration should look similar to the following json snippet:
```json
{
  "MaxTransferErrorAmount": 100,
  "DefaultLanguageCode": "en",
  "SmtpHost": "mySmtpHost",
  "SmtpPort": 25,
  "SmtpEnableSsl": true,
  "SmtpUserName": "USERNAME",
  "SmtpUserPassword": "XXXXXX",
  "MailFrom": "email@domain.org",
  "DotStatSuiteCoreCommonDbConnectionString": "Data Source=localhost;Initial Catalog=COMMON_DB;User ID=USERNAME;Password=XXXXXX",
  "spacesInternal": [
    {
      "Id": "design",
      "DotStatSuiteCoreStructDbConnectionString": "Data Source=localhost;Initial Catalog=STRUCTURE_DB;User ID=USERNAME;Password=XXXXXX",
      "DotStatSuiteCoreDataDbConnectionString": "Data Source=localhost;Initial Catalog=DATA_DB;User ID=USERNAME;Password=XXXXXX",
      "DataImportTimeOutInMinutes": 60,
      "AuthEndpoint": "",
      "DatabaseCommandTimeoutInSec": 360
    },
    {
      "Id": "disseminate",
      "DotStatSuiteCoreStructDbConnectionString": "Data Source=localhost;Initial Catalog=STRUCTURE_DB;User ID=USERNAME;Password=XXXXXX",
      "DotStatSuiteCoreDataDbConnectionString": "Data Source=localhost;Initial Catalog=DATA_DB;User ID=USERNAME;Password=XXXXXX",
      "DataImportTimeOutInMinutes": 60,
      "AuthEndpoint": "",
      "DatabaseCommandTimeoutInSec": 360
    }
  ]
}
```
5. Save the configuration
6. Restart the website 